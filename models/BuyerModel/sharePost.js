const mongoose = require("mongoose");
const {
  labelSchema,
  variationSchema,
  ratingSchema,
  likeSchema,
} = require("../partial/partialSchemaForPost.js");

const sharePostSchema = new mongoose.Schema(
  {
    orderid: {
      type: mongoose.Types.ObjectId,
      ref: "Order",
      index: true
    },
    itemid: {
      type: mongoose.Types.ObjectId,
      ref: "ProductPost",
      index: true
    },
    buyerid: {
      type: mongoose.Types.ObjectId,
      ref: "Buyer",
      index: true
    },
    likeCount:{
        type: Number,
        default: 0
    },
    state: {
      type: String,
      enum: ["private", "public"],
      default: "public",
    },
    name: {
      type: String
    },
    content: {
      type: String,
    },
    likeCount:{
      type: Number,
      default: 0
    },
    images: [String],
    videoes: [String]
  },
  {
    timestamps: true,
    autoIndex: true,
  }
);

sharePostSchema.statics.isPostExist = async function (buyerid, orderid, itemid) 
{
  const sharePost = await this.findOne({
    buyerid, orderid, itemid
  }).exec();
  if(sharePost!==null)
  {
    return true;
  }
  return false;
};

sharePostSchema.statics.getSharePosts = async function(sharePostGetDto){
    const sharePosts = await this.find({buyerid: sharePostGetDto.buyerid}).skip(sharePostGetDto.skip).limit(sharePostGetDto.limit).exec();
    if(!sharePosts){
        return null;
    }
    return sharePosts;
}

sharePostSchema.statics.getItemSharePosts = async function(getItemShareDto){
    const sharePosts = await this.find({itemid: getItemShareDto.itemid}).skip(getItemShareDto.skip).limit(getItemShareDto.limit);
    if(!sharePosts){
        return null;
    }
    return sharePosts;
}

sharePostSchema.statics.getSharePost = async function(postid){
    const sharePost = await this.findOne({_id: postid}).exec();
    return sharePost;
}

const SharePost = mongoose.model("SharePost", sharePostSchema);
module.exports = SharePost;
