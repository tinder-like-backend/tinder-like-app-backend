const mongoose = require("mongoose");

const sharePostCommentReplySchema = new mongoose.Schema(
    {
        commentId: {
            type: mongoose.Types.ObjectId,
            required: true,
            index: true,
            ref: "SharePostComment"
        },
        userId:{
            type: mongoose.Types.ObjectId,
            required: true
        },
        content:{
            type: String,
            maxLength: [100, "Comment must be less than 100"],
            required: true
        },
        isShowed: {
            type: Number,
            enum: [0, 1], // 1 => showed 
            default: 1
        }
    },
    {
        timestamp: true
    }
)

sharePostCommentReplySchema.statics.addAndGetReply = async function(shareCommentReplyDto){
    try{
        const result = await this.create(shareCommentReplyDto)
        return result
    }
    catch(err)
    {
        console.log(err);
    }
}

const SharePostCommentReply = mongoose.model("SharePostCommentReply", sharePostCommentReplySchema);
module.exports = SharePostCommentReply;