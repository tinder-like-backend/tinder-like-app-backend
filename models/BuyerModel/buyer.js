const mongoose = require("mongoose");
const validator = require("validator");

const {
  buyerFollowSchema,
  shopFollowSchema,
  likeItemSchema,
  tinderItemSchema,
} = require("../partial/partialSchema.js");

const buyerSchema = new mongoose.Schema(
  {
    role: {
      type: String,
      default: "buyer",
    },
    awardCoin: {
      type: Number,
      default: 0,
    },
    account: {
      type: String,
      required: true,
      index: true,
    },
    email: {
      type: String,
      trim: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Email is invalid");
        }
      },
      index: true
    },
    password: {
      type: String,
      required: false,
    },
    salt: {
      type: String
    },
    verify_token:{
      type: String
    },
    verify_expire:{
      type: String
    },
    active:{
      type: Boolean
    },
    gender:{
      type: String
    },
    selfIntro: {
      type: String,
    },
    public:{
      type: Boolean,
      default: true
    },
    followerCount: {
      type: Number,
      default: 0,
    },
    followingCount: {
      type: Number,
      default: 0,
    },
    superLikeLeft: {
      type: Number,
      default: 10,
    },
    profilePic: {
      type: String,
    },
    birthday: {
      type: String
    },
    likedItems: {
      type: [String]
    },
    resetToken:{
      type: String
    },
    resetTokenExpire:{
      type: String
    },
    refreshToken: String,
  }, 
  {
    timestamps: true,
  }
);

buyerSchema.virtual("sharePosts", {
  ref: "SharePost",
  localField: "_id",
  foreignField: "buyerid",
});

buyerSchema.virtual("orders", {
  ref: "Order",
  localField: "_id",
  foreignField: "buyerid",
});

buyerSchema.statics.isBuyerExist = async function(buyerId){
    const buyer = await this.findOne({_id: buyerId});
    if(buyer){
        return true;
    }
    else{
        return false;
    }
}

buyerSchema.statics.getAccountAndPic = async function(buyerId){
    const buyer = await this.findOne({_id: buyerId});
    if(buyer){
        if(!buyer.profilePic.startsWith('http')) buyer.profilePic = 'https://cf.shopee.tw/file/'+buyer.profilePic
        return {
            account: buyer.account,
            profilePic: buyer.profilePic
        }
    }
    return false;
}

buyerSchema.statics.getBuyer = async function(buyerId){
    return await this.findOne({_id: buyerId});
}

const Buyer = mongoose.model("Buyer", buyerSchema);
module.exports = Buyer;

