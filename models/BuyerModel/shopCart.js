const mongoose = require("mongoose");
const ProductPost = require("../ShopModel/productPost.js")
const Shop = require("../ShopModel/shop.js")

const shopCartSchema = mongoose.Schema(
  {
    buyerid: {
      type: mongoose.Types.ObjectId,
      ref: "Buyer",
      index: true,
    },
    cartDetails: [
      {
        type: new mongoose.Schema(
            {
                itemid: {
                    type: mongoose.Types.ObjectId,
                    ref: "ProductPost",
                    index: true,
                    required: true
                },
                fromSharePost: {
                    type: mongoose.Types.ObjectId,
                    ref: "SharePost",
                    index: true,
                    default: null,
                },
                amount: { type: Number },
                _id: false,
            },
            {
                timestamps: true
            }
        )
      }
    ]
  }
);

shopCartSchema.statics.addToCart = async function (itemToCart) {
    // 先找找對應的buyer跟shopid有無在DB中
    let cartExist = await this.findOne({buyerid: itemToCart.buyerid}).exec();
    if(cartExist){
        let itemExist = false;
        cartExist.cartDetails.forEach(item => {
            if(item.itemid.toString() === itemToCart.itemid){
                item.amount += itemToCart.amount;
                itemExist = true;
                return;
            }
        });
        if(!itemExist){
            cartExist.cartDetails.push({
                itemid: itemToCart.itemid,
                amount: itemToCart.amount,
                fromSharePost: itemToCart.fromSharePost
            })
        }
        
        await cartExist.save()
        return cartExist;
    }
    else{
        const newCart = new ShopCart({buyerid: itemToCart.buyerid})
        newCart.cartDetails.push({
            itemid: itemToCart.itemid,
            amount: itemToCart.amount,
            cost: itemToCart.cost,
            fromSharePost: itemToCart.fromSharePost
        })
        await newCart.save();
        return newCart;
    }
}

shopCartSchema.statics.getCarts = async function(buyerid, limit=5, skip=0){
    let cartItems = await this.findOne({buyerid: buyerid}, {cartDetails: {$slice: [skip, limit]}})
    if(cartItems){
        for(let item of cartItems.cartDetails){
            // const itemDetail = await ProductPost.findOne(
            //     {_id: item.itemid}, 
            //     {_id: 0, shopAccount: 1, name: 1, price: 1, images: {$slice: 1}}
            // ).exec();
            // item._doc.shopAccount = itemDetail.shopAccount;
            // item._doc.itemName = itemDetail.name;
            // item._doc.totalCost = itemDetail.price * item.amount;

            const itemDetail = await ProductPost.findOne({_id: item.itemid});

            if(!itemDetail.images[0].startsWith('http')){
                item._doc.image = 'https://cf.shopee.tw/file/'+itemDetail.images[0];
            }
            else item._doc.image = itemDetail.images[0];

            item._doc.item = itemDetail;
        }
        return cartItems;
    }
    return null;
}

shopCartSchema.statics.modifyAmount = async function(patchAmount){
    let cart = await this.findOne({buyerid: patchAmount.buyerid}).exec();
    for(item of cart.cartDetails){
        if(item.itemid.toString() === patchAmount.itemid){
            item.amount = patchAmount.amount;
            break;
        }
    }
    await cart.save();
    return cart;
}

shopCartSchema.statics.removeItem = async function(buyerid, itemid){
    let cart = await this.findOne({buyerid: buyerid});
    if(cart === null){
        return;
    }

    let newCart = cart.cartDetails.filter(item => item.itemid.toString() != itemid.toString());
  
    cart.cartDetails = newCart;
    await cart.save()
}

const ShopCart = mongoose.model("ShopCart", shopCartSchema);
module.exports = ShopCart;
