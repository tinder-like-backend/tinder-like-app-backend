const mongoose = require('mongoose');

const deliveryMethodSchema = new mongoose.Schema(
    {
        deliver: {
            type: string,
            index: true
        },
        description: {
            type: String
        },
        location: {
            type: string
        },
        price: {
            type: Number
        },
        shortName: {
            type: String
        }
    }
)

const DeliveryMethod = mongoose.model("DeliveryMethod", deliveryMethodSchema)
module.exports = DeliveryMethod