const mongoose = require('mongoose');
const Buyer = require("./buyer.js");
const Shop = require("../ShopModel/shop.js");
const FollowReturnDTO = require('../../DTO/FollowReturnDTO');

// followType: 0表示買家追蹤買家，1表示買家追蹤賣家
// state表示有無接受追蹤
const followSchema = new mongoose.Schema(
  {
    followerid:{
      type: mongoose.Types.ObjectId,
      index: true
    },
    followingid: {
      type: mongoose.Types.ObjectId,
      index: true
    },
    followType:{
      type: Number,
      enum: [0, 1],
      required: true
    }
  },
  {
    timestamps: true
  }
)

followSchema.statics.isFollowExist = async function(followerid, followingid){
    const result = await this.findOne({followerid: followerid, followingid: followingid}).exec()
    if(result){
        return true
    }
    return false
}

followSchema.statics.findFollowing = async function(followingid, followType){
    if(followType === 0){ // 0表示買家追蹤買家
        return await Buyer.findOne({_id: followingid});
    }else{ // 1表示買家追蹤賣家
        return await Shop.findOne({_id: followingid});
    }
}

followSchema.statics.findFollower = async function(followerid, role){
    if(role==='buyer'){
        return await Buyer.findOne({_id: followerid});
    }else{
        return await Shop.findOne({_id: followerid});
    }
}

followSchema.statics.removeFollow = async function(followerid, followingid){
    let follow = await this.deleteOne({followerid: followerid, followingid: followingid}).exec();

    return follow;
}

followSchema.statics.getFollowings = async function(followerid, skip, limit){
    let followings = await this.find({followerid: followerid}, {_id: 0, followingid: 1, followType: 1}, {skip: skip, limit: limit}).exec();
    let result = [];
    for(following of followings){
        if(following.followType === 0){
            let buyer = await Buyer.findOne({_id: following.followingid});
            if(!buyer.profilePic.startsWith("https") && !buyer.profilePic.startsWith("http")){
                buyer.profilePic = "https://cf.shopee.tw/file/"+buyer.profilePic
            }
            // Object.assign(following._doc, buyer);
            result.push(new FollowReturnDTO(followInfo=following, user=buyer));
        }else{
            let shop = await Shop.findOne({_id: following.followingid});
            // Object.assign(following, shop);
            result.push(new FollowReturnDTO(followInfo=following, user=shop));
        }
    }
    return result;
}

followSchema.statics.getFollowers = async function(followingid, skip, limit){
    let followers = await this.find({followingid: followingid}, {_id: 0, followerid: 1, followType: 1}, {skip: skip, limit: limit}).exec();
    let result = [];
    for(follower of followers){
        if(follower.followType === 0){
            let buyer = await Buyer.findOne({_id: follower.followerid});
            if(!buyer.profilePic.startsWith("https") && !buyer.profilePic.startsWith("http")){
                buyer.profilePic = "https://cf.shopee.tw/file/"+buyer.profilePic
            }
            //Object.assign(follower._doc, buyer);
            result.push(new FollowReturnDTO(followInfo=follower, user=buyer));
        }else{
            let shop = await Shop.findOne({_id: follower.followerid});
            //Object.assign(follower, shop);
            result.push(new FollowReturnDTO(followInfo=follower, user=shop));
        }
    }
    return result;
}

const Follow = mongoose.model("Follow", followSchema);
module.exports = Follow;