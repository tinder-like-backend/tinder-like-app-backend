const mongoose = require('mongoose')

const addressSchema = mongoose.Schema(
    {
        buyerid: {
            type: mongoose.Types.ObjectId,
            index: true
        },
        zipCode: {
            type: String,
            index: true
        },
        country: {
            type: String,
            index: true
        },
        city: {
            type: String,
            index: true            
        },
        district: {
            type: String,
            index: true
        },
        lane: {
            type: String,
            index: true
        },
        number: {
            type: Number
        },
        remark: {
            type: String
        }
    }
)