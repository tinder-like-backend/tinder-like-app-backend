const mongoose = require("mongoose");

const likedSharePostSchema = new mongoose.Schema(
    {
        buyerid:{
            type: mongoose.Schema.Types.ObjectId,
            index: true
        },
        postid:{
            type: mongoose.Schema.Types.ObjectId,
            index: true
        }
    },
    {
        timestamps: true
    }
)

likedSharePostSchema.statics.ifLikeExist = async function(buyerid, postid){
    const like = await this.findOne({buyerid: buyerid, postid: postid});
    if(like){
        return true;
    }
    return false;
}

const LikedSharePost = mongoose.model("LikedSharePost", likedSharePostSchema);
module.exports = LikedSharePost;