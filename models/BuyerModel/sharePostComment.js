const mongoose = require("mongoose");

const sharePostCommentSchema = new mongoose.Schema(
    {
        commentorId: {
            type: mongoose.Types.ObjectId,
            required: true,
            index: true
        },
        postId: {
            type: mongoose.Types.ObjectId,
            required: true,
            index: true
        },
        comment:{
            type: String,
            maxLength: [150, "Comment must be less than 150"],
            required: true
        },
        likeCount:{
            type: Number,
            default: 0
        },
        isShowed: {
            type: Number,
            enum: [0, 1], // 1 => showed 
            default: 1
        }
    },
    {
        timestamps: true
    }
)

sharePostCommentSchema.virtual("sharePosts", {
    ref: "SharePost",
    localField: "postId",
    foreignField: "_id",
  });
  
sharePostCommentSchema.virtual("buyer", {
    ref: "Buyer",
    localField: "commentorId",
    foreignField: "_id",
});

sharePostCommentSchema.statics.getCommentandUserDetail = async function(postid, limit, skip){
    const comments = await this.find({postId: postid}, 'commentorId comment createdAt likeCount').skip(skip).limit(limit).exec();
    return comments;
}

const SharePostComment = mongoose.model("SharePostComment", sharePostCommentSchema);
module.exports = SharePostComment;