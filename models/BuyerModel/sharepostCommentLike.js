const mongoose = require("mongoose");

const sharePostCommentLikeSchema = new mongoose.Schema(
    {
        commentId: {
            type: mongoose.Types.ObjectId,
            required: true,
            index: true,
            ref: "SharePostComment"
        },
        userId:{
            type: mongoose.Types.ObjectId,
            required: true
        }
    },
    {
        timestamp: true
    }
)

sharePostCommentLikeSchema.statics.isLikeExist = async function(commentId, userId) {
    const result = await this.findOne({commentId, userId}).exec();
    if(result === null) {
        return false;
    }
    return true;
}

sharePostCommentLikeSchema.statics.addLike = async function(commentId, userId){
    try{
        const result = await this.updateOne({commentId, userId}, {commentId, userId}, {upsert: true})
        return result;
    }
    catch(err){
        console.log(err)
    }
}

sharePostCommentLikeSchema.statics.removeLike = async function(commentId, userId){
    try{
        await this.deleteOne({commentId, userId});
    }
    catch(err){
        console.log(err)
    }
}

const SharePostCommentLike = mongoose.model("SharePostCommentLike", sharePostCommentLikeSchema);
module.exports = SharePostCommentLike;