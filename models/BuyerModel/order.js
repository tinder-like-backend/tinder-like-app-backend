const mongoose = require("mongoose");
const ProductPost = require("../ShopModel/productPost.js")
const ShopCart = require("./shopCart.js")

const orderSchema = new mongoose.Schema(
  {
    buyerid: {
      type: mongoose.Types.ObjectId,
      ref: "Buyer",
      index: true
    },
    orderDetails: [
      {
        itemid: {
          type: mongoose.Types.ObjectId,
          ref: "ProductPost",
          index: true
        },
        fromSharePost: {
            type: mongoose.Types.ObjectId,
            ref: "SharePost",
            index: true,
            default: null
          },
        amount: { type: Number },
        totalCost: { type: Number },
        _id: false
      }
    ],
    orderStatus: {
        type: String,
        enum: ["備貨中","賣家已出貨","運送中","已完成訂單"],
        default: "備貨中"
    },
    payingStatus: {
        type: String,
        enum: ["已付款","尚未付款"],
        default: "已付款"
    }
  },
  {
    timestamps: true,
  }
);
  
orderSchema.statics.checkOrderOfBuyer = async function (orderid) {
    const order = await this.findOne({ _id: orderid }).exec();
    if (order !== null) {
        return true;
    } else {
        return false;
    }
};

orderSchema.statics.getOrder = async function(orderid){
    let orderItems = await this.findOne({_id: orderid}).exec()
    if(orderItems){
        for(let item of orderItems.orderDetails){
            const itemDetail = await ProductPost.findOne({_id: item.itemid});

            if(!itemDetail.images[0].startsWith('http')){
                item._doc.image = 'https://cf.shopee.tw/file/'+itemDetail.images[0];
            }
            else item._doc.image = itemDetail.images[0];

            item._doc.item = itemDetail;
        }
        return orderItems;
    }
    return null;
}

// 將訂單跟ProductPost結合避免dirty，訂單創建成功並刪掉購物車
orderSchema.statics.checkOrder = async function(order){
    let newOrder = new Order({buyerid: order.buyerid})

    for(let i = 0; i < order.orderItems.length; i++){
        let item = await ProductPost.findOne({_id: order.orderItems[i].itemid});
        if(item === null){
            return {flag: false, msg: "product not found", order: []}
        }
        item.stock = item.stock - order.orderItems[i].amount;
        if(item.stock < 0){
            return {flag: false, msg: `${item.name} is not enough`, order: []}
        }
        
        // 數量足夠，計算價錢
        order.orderItems[i].totalCost = order.orderItems[i].amount * item.price;
        newOrder.orderDetails.push(order.orderItems[i])
    }
    
    for(let i = 0; i < order.orderItems.length; i++){
        let item = await ProductPost.findOne({_id: order.orderItems[i].itemid});
        item.stock = item.stock - order.orderItems[i].amount;
        await item.save();
    }
    await newOrder.save();
    
    return {flag: true, msg: "Create Order Success", order: newOrder};
}

const Order = mongoose.model("Order", orderSchema);
module.exports = Order;