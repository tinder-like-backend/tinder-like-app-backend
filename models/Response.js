class Response {
  constructor(status, code, message, data = {}) {
    this.status = status;
    this.code = code;
    this.message = message;
    this.data = data;
  }
}

module.exports = Response;
