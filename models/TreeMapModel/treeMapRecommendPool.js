const mongoose = require("mongoose");

const treeMapRecommendPoolSchema = new mongoose.Schema(
  {
    buyerid: {
      type: mongoose.Types.ObjectId,
      ref: "Buyer",
      index: true,
    },
    itemid: {
      type: mongoose.Types.ObjectId,
      ref: "ProductPost",
      index: true,
    },
    name: {
      type: String,
    },
    labels: [
      {
        labelid: Number,
        display_name: {
          type: String,
          index: true,
        },
        _id: false,
      },
    ],
    feLabels: [
      {
        labelid: Number,
        display_name: {
          type: String,
        },
        _id: false,
      },
    ],
    score: {
      type: Number,
      index: true,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

treeMapRecommendPoolSchema.virtual("itemsSize").get(function () {
  return this.items.length;
});

treeMapRecommendPoolSchema.statics.tinderLike = async function (
  buyerid,
  labels,
  feLabels,
  labelPoint,
  feLabelPoint
) {
  const res = await this.updateMany(
    { buyerid, "labels.display_name": { $in: labels } },
    [
      {
        $project: {
          buyerid: 1,
          itemid: 1,
          name: 1,
          labels: 1,
          feLabels: 1,
          score: {
            $add: [
              {
                $multiply: [
                  {
                    $divide: [
                      {
                        $size: {
                          $setIntersection: ["$labels.display_name", labels],
                        },
                      },
                      labels.length,
                    ],
                  },
                  labelPoint,
                ],
              },
              {
                $multiply: [
                  {
                    $divide: [
                      {
                        $size: {
                          $setIntersection: [
                            "$feLabels.display_name",
                            feLabels,
                          ],
                        },
                      },
                      feLabels.length,
                    ],
                  },
                  feLabelPoint,
                ],
              },
              {
                $cond: {
                  if: {
                    $gte: ["$score", 0],
                  },
                  then: "$score",
                  else: 0,
                },
              },
            ],
          },
        },
      },
      {
        $set: { score: "$score" },
      },
    ]
  );

  return res;
};

treeMapRecommendPoolSchema.statics.countItemsAndUpdateByTime = async function (
  buyerid,
  penalty,
  unit
) {
  const res = await this.updateMany({ buyerid }, [
    {
      $project: {
        buyerid: 1,
        itemid: 1,
        name: 1,
        labels: 1,
        feLabels: 1,
        score: {
          $add: [
            "$score",
            {
              $multiply: [
                penalty,
                {
                  $dateDiff: {
                    startDate: "$updatedAt",
                    endDate: new Date(),
                    unit: unit,
                  },
                },
              ],
            },
          ],
        },
      },
    },
  ]);

  return res;
};

treeMapRecommendPoolSchema.statics.getDelItems = async function (
  buyerid,
  delAmount
) {
  const items = await this.aggregate([
    {
      $match: {
        buyerid: new mongoose.Types.ObjectId(buyerid),
      },
    },
    {
      $sort: {
        score: 1,
      },
    },
    {
      $limit: delAmount,
    },
    {
      $project: {
        _id: 1,
      },
    },
  ]);

  return items;
};

treeMapRecommendPoolSchema.statics.getTreemapItems = async function (
  buyerid,
  size
) {
  const itemids = await this.aggregate([
    {
      $match: {
        buyerid: new mongoose.Types.ObjectId(buyerid),
      },
    },
    {
      $sample: {
        size: size,
      },
    },
    {
      $project: {
        _id: 0,
        itemid: 1,
        score: 1,
      },
    },
    {
      $sort: {
        score: -1,
      },
    },
  ]);

  return itemids;
};

treeMapRecommendPoolSchema.statics.updateWhenShowOnTreeMap = async function (
  buyerid,
  itemids,
  point
) {
  await this.updateMany({ buyerid, itemid: { $in: itemids } }, [
    {
      $project: {
        buyerid: 1,
        itemid: 1,
        name: 1,
        labels: 1,
        feLabels: 1,
        score: {
          $add: ["$score", point],
        },
      },
    },
  ]);
};

treeMapRecommendPoolSchema.statics.updateByStayingTimeOnPost = async function (
  buyerid,
  labels,
  feLabels,
  labelPoint,
  feLabelPoint,
  stayingTime
) {
  const res = await this.updateMany(
    { buyerid, "labels.display_name": { $in: labels } },
    [
      {
        $project: {
          buyerid: 1,
          itemid: 1,
          name: 1,
          labels: 1,
          feLabels: 1,
          score: {
            $add: [
              {
                $multiply: [
                  {
                    $divide: [
                      {
                        $size: {
                          $setIntersection: ["$labels.display_name", labels],
                        },
                      },
                      labels.length,
                    ],
                  },
                  labelPoint,
                ],
              },
              {
                $multiply: [
                  {
                    $divide: [
                      {
                        $size: {
                          $setIntersection: [
                            "$feLabels.display_name",
                            feLabels,
                          ],
                        },
                      },
                      feLabels.length,
                    ],
                  },
                  feLabelPoint,
                ],
              },
              {
                $cond: {
                  if: {
                    $gte: ["$score", 0],
                  },
                  then: "$score",
                  else: 0,
                },
              },
            ],
          },
        },
      },
      {
        $set: { score: "$score" },
      },
    ]
  );

  return res;
};

const TreeMapRecommendPool = mongoose.model(
  "TreeMapRecommendPool",
  treeMapRecommendPoolSchema
);
module.exports = TreeMapRecommendPool;
