const mongoose = require('mongoose');

const searchHistorySchema = mongoose.Schema(
    {
        userId: {
            type: mongoose.Types.ObjectId,
            index: true
        },
        keywords: [
            {
                word: String,
                searchDate: {
                    type: Date,
                    default: Date.now,
                    index: true
                }
            }
        ]
    }
)


searchHistorySchema.statics.addKeyword = async function(userId, keyword){
    const history = await this.findOne({userId: userId}).exec();
    if(history){
        let isHasKeyword = false;
        for(let word of history.keywords){
            if(word.word===keyword.trim()){
                isHasKeyword = true;
                break;
            }
        }
        if(!isHasKeyword){
            history.keywords.push({
                word: keyword
            })
            await history.save();
        }
    }
    else{
        let keywords = [];
        keywords.push({word: keyword})
        const searchHistory = new SearchHistory({userId, keywords})
        await searchHistory.save()
    }
}

searchHistorySchema.statics.getKeywords = async function(userId, limit=null, skip=0){
    if(limit === null){
        const keywords = await this.findOne({userId: userId}, {_id: 0, userId: 1, keywords: 1}).exec();
        return keywords
    }
    const keywords = await this.find({userId: userId}, {_id: 0, userId: 1, keywords: {$slice: [skip, limit]}}).exec();
    return keywords;
}

searchHistorySchema.statics.removeKeyword = async function(userId, keywordId){
    let searchHistory = await this.findOne({userId: userId}).exec();
    let keywords = searchHistory.keywords.filter(keyword => keyword._id.toString() !== keywordId)
    searchHistory.keywords = keywords;
    await searchHistory.save()
}

searchHistorySchema.statics.clear = async function(userId){
    let searchHistory = await this.findOne({userId: userId}).exec();
    searchHistory.keywords = [];
    await searchHistory.save();
}

const SearchHistory = mongoose.model("SearchHistory", searchHistorySchema);
module.exports = SearchHistory;