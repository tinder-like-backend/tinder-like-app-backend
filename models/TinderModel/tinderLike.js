const mongoose = require('mongoose');
const {
  labelSchema
} = require("../partial/partialSchemaForPost.js");

const tinderLikeItemSchema = new mongoose.Schema(
  {
    buyerid: {
      type: mongoose.Types.ObjectId,
      ref: 'Buyer',
      index: true
    },
    itemid: {
      type: mongoose.Types.ObjectId,
      ref: 'ProductPost',
      index: true
    },
    name: {
      type: String,
    },
    labels: [String],
    feLabels: {
      type: [String],
      default: [],
    },
  },{
    timestamp: true
  }
)

const TinderLikeItem = mongoose.model("TinderLikeItem", tinderLikeItemSchema)
module.exports = TinderLikeItem;