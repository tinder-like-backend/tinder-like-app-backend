const mongoose = require('mongoose');

const tinderRecommendPoolSchema = new mongoose.Schema(
    {
      buyerid: {
        type: mongoose.Types.ObjectId,
        ref: 'Buyer',
        index: true
      },
      items: [
        {
          itemid: {
            type: mongoose.Types.ObjectId,
            ref: 'ProductPost'
          },
          labels: [
            {
              labelid: Number,
              display_name: {
                type: String,
                index: true
              }
            }
          ],
          feLabels: [
            {
              labelid: Number,
              display_name: {
                type: String,
                index: true
              }
            }
          ]
        }
      ]
    },
    {
      timestamp: true
    }
)

const TinderRecommendPool = mongoose.model("TinderRecommendPool", tinderRecommendPoolSchema)
module.exports = TRecommendPool;