const mongoose = require("mongoose");

const productPostCommentReplySchema = new mongoose.Schema(
    {
        commentId: {
            type: mongoose.Types.ObjectId,
            required: true,
            index: true,
            ref: "ProductPostComment"
        },
        userId:{
            type: mongoose.Types.ObjectId,
            required: true
        },
        content:{
            type: String,
            maxLength: [100, "Comment must be less than 100"],
            required: true
        },
        isShowed: {
            type: Number,
            enum: [0, 1], // 1 => showed 
            default: 1
        }
    },
    {
        timestamp: true
    }
)

productPostCommentReplySchema.statics.addAndGetReply = async function(productCommentReplyDto){
    try{
        const result = await this.create(productCommentReplyDto)
        return result
    }
    catch(err)
    {
        console.log(err);
    }
}

const ProductPostCommentReply = mongoose.model("ProductPostCommentReply", productPostCommentReplySchema);
module.exports = ProductPostCommentReply;