const mongoose = require("mongoose");
const { tinderUserSchema } = require("../partial/partialSchema.js");
const TreeMapRecommendPool = require("../TreeMapModel/treeMapRecommendPool.js");
const {
  labelSchema,
  variationSchema,
  ratingSchema,
} = require("../partial/partialSchemaForPost.js");

const productPostSchema = new mongoose.Schema(
  {
    shopid: {
      required: true,
      type: mongoose.Types.ObjectId,
      ref: "Shop",
    },
    shopAccount: {
      type: String,
      index: true,
      ref: "Shop",
    },
    sp_itemid: {
      type: Number,
    },
    sp_shopid: {
      type: Number,
    },
    name: {
      type: String,
      required: true,
    },
    seg_name: {
      type: String,
    },
    content: {
      type: String,
    },
    labels: [labelSchema],
    feLabels: {
      type: [labelSchema],
      default: [
        {
          labelid: 0,
          display_name: "Null",
        },
      ],
    },
    shipping_free: {
      type: Boolean,
      default: false,
    },
    variation: [variationSchema],
    models: [
      {
        name: String,
        price: Number,
        stock: Number,
        modelid: Number,
        _id: false,
      },
    ],
    images: [String],
    display: {
      type: Boolean,
      default: true
    },
    price: Number,
    priceMax: Number,
    priceMin: Number,
    rating: ratingSchema,
    discount: Number,
    historicalSold: Number,
    monthSold: Number,
    stock: Number,
    likeCount: {
        type: Number,
        default: 0
    },
    shareCount: {
        type: Number,
        default: 0
    },
    tinderLikerCount: {
        type: Number,
        default: 0
    },
    tinderUnlikeCount: {
        type: Number,
        default: 0
    },
    tinderSuperlikeCount: {
        type: Number,
        default: 0
    },
  },
  {
    timestamps: true,
  }
);

productPostSchema.virtual("ratingDetail", {
  ref: "Rating",
  localField: "_id",
  foreignField: "itemid",
});

productPostSchema.virtual("sharePosts", {
  ref: "SharePost",
  localField: "_id",
  foreignField: "itemid",
});

// get random products
productPostSchema.static.getRandomItems = async function (size) {
  const products = await this.aggregate([
    { $sample: { size: parseInt(size) } },
  ]);
  return products;
};

// tinderlike: output about 1000 items with score to TreeMapRecommendPool
productPostSchema.statics.tinderLike = async function (
  buyerid,
  labels,
  feLabels,
  existItems,
  labelPoint,
  feLabelPoint,
  limit
) {
  const recommendProducts = await this.aggregate([
    {
      $match: {
        "labels.display_name": {
          $in: labels,
        },
        _id: {
          $nin: existItems,
        },
      },
    },
    {
      $sample: {
        size: 2000,
      },
    },
    {
      $project: {
        _id: 0,
        itemid: "$_id",
        buyerid: buyerid,
        name: 1,
        labels: 1,
        feLabels: 1,
        labelJoinSize: 1,
        feLabelJoinSize: 1,
        score: {
          $add: [
            {
              $multiply: [
                {
                  $divide: [
                    {
                      $size: {
                        $setIntersection: ["$labels.display_name", labels],
                      },
                    },
                    labels.length,
                  ],
                },
                labelPoint,
              ],
            },
            {
              $multiply: [
                {
                  $divide: [
                    {
                      $size: {
                        $setIntersection: ["$feLabels.display_name", feLabels],
                      },
                    },
                    feLabels.length,
                  ],
                },
                feLabelPoint,
              ],
            },
            {
              $cond: {
                if: {
                  $gte: ["$score", 0],
                },
                then: "$score",
                else: 0,
              },
            },
          ],
        },
      },
    },
    {
      $sort: {
        score: -1,
      },
    },
    {
      $limit: limit,
    },
  ]);

  return recommendProducts;
};

productPostSchema.statics.isItemExist = async function(itemId){
    const product = await this.findOne({_id: itemId});
    if(product){
        return true;
    }
    else{
        return false;
    }
}

productPostSchema.statics.getItemById = async function(itemId, projectField){
    const result = await this.findOne({_id: itemId}, projectField);
    // console.log(result)
    return result
}

productPostSchema.statics.getItemByIdWithSelect = async function(itemId, projectField){
    const result = await this.findOne({_id: itemId}).select(projectField);
    // console.log(result)
    return result
}


const ProductPost = mongoose.model("ProductPost", productPostSchema);
module.exports = ProductPost;
