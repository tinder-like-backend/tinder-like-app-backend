const mongoose = require("mongoose");
const {
  buyerFollowSchema,
  shopFollowSchema,
} = require("../partial/partialSchema");

const shopSchema = new mongoose.Schema(
  {
    buyerid: {
      type: mongoose.Types.ObjectId,
      ref: 'Buyer'
    },
    sp_shopid: {
      type: Number,
      default: 0,
    },
    role: {
      type: String,
      deault: "shop",
    },
    shopAccount: {
      type: String,
      index: true,
    },
    email: {
      type: String,
      index: true,
    },
    itemcount: Number,
    profilePic: {
      type: String,
    },
    selfIntro: {
      type: String,
    },
    rating: {
      ratingStar: {
        type: Number,
        index: true,
      },
      ratingBad: {
        type: Number,
      },
      ratingNormal: {
        type: Number,
      },
      ratingGood: {
        type: Number,
      },
    },
    likeCount: {
      type: Number,
      default: 0,
    },
    public:{
      type: Boolean,
      default: true
    },
    followerCount: {
      type: Number,
      default: 0,
    },
    followingCount: {
      type: Number,
      default: 0,
    },
    birthday: {
      type: String
    }
  },
  {
    timestamps: true,
  }
);

shopSchema.virtual("buyer", {
  ref: "Buyer",
  localField: "buyerid",
  foreignField: "_id",
});

shopSchema.statics.isShopExist = async function(sohpid){
    const shop = await this.findOne({_id: sohpid});
    if(shop){
        return true;
    }
    return false;
}

shopSchema.statics.getAccountAndPic = async function(shopid){
    const shop = await this.findOne({_id: shopid});
    if(shop){
        if(!shop.profilePic.startsWith('http')) shop.profilePic = 'https://cf.shopee.tw/file/'+shop.profilePic
        return {
            account: shop.shopAccount,
            profilePic: shop.profilePic
        }
    }
    return false;
}

shopSchema.statics.ifShopExist = async function(shopid){
    const shop = await this.findOne({_id: shopid}).exec();
    if(shop){
        return true;
    }
    return false;
}

const Shop = mongoose.model("Shop", shopSchema);
module.exports = Shop;
