const mongoose = require("mongoose");

const productPostCommentLikeSchema = new mongoose.Schema(
    {
        commentId: {
            type: mongoose.Types.ObjectId,
            required: true,
            index: true,
            ref: "ProductPostComment"
        },
        userId:{
            type: mongoose.Types.ObjectId,
            required: true
        }
    },
    {
        timestamp: true
    }
)

productPostCommentLikeSchema.statics.isLikeExist = async function(commentId, userId) {
    const result = await this.findOne({commentId, userId}).exec();
    if(result === null) {
        return false;
    }
    return true;
}

productPostCommentLikeSchema.statics.addLike = async function(commentId, userId){
    try{
        const result = await this.updateOne({commentId, userId}, {commentId, userId}, {upsert: true})
        return result;
    }
    catch(err){
        console.log(err)
    }
}

productPostCommentLikeSchema.statics.removeLike = async function(commentId, userId){
    try{
        await this.deleteOne({commentId, userId});
    }
    catch(err){
        console.log(err)
    }
}

const ProductPostCommentLike = mongoose.model("ProductPostCommentLike", productPostCommentLikeSchema);
module.exports = ProductPostCommentLike;