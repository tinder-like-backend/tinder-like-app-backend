const mongoose = require("mongoose");

const productPostCommentSchema = new mongoose.Schema(
    {
        commentorId: {
            type: mongoose.Types.ObjectId,
            required: true,
            index: true,
            // ref: 'Buyer'
        },
        itemId: {
            type: mongoose.Types.ObjectId,
            required: true,
            index: true
        },
        comment:{
            type: String,
            maxLength: [150, "Comment must be less than 150"],
            required: true
        },
        likeCount:{
            type: Number,
            default: 0
        },
        isShowed: {
            type: Number,
            enum: [0, 1], // 1 => showed 
            default: 1
        }
    },
    {
        timestamps: true
    }
)

productPostCommentSchema.virtual("sharePosts", {
    ref: "ProductPost",
    localField: "itemId",
    foreignField: "_id",
  });
  
productPostCommentSchema.virtual("buyer", {
    ref: "Buyer",
    localField: "commentorId",
    foreignField: "_id",
});

productPostCommentSchema.statics.getCommentandUserDetail = async function(itemId, limit, skip){
    const comments = await this.find({itemId: itemId}).skip(skip).limit(limit).exec();
    return comments;
}

const ProductPostComment = mongoose.model("ProductPostComment", productPostCommentSchema);
module.exports = ProductPostComment;