const mongoose = require('mongoose')
const os = require('os');

let dbName = 'tinderAppDB_v2'
if(os.hostname()==='DESKTOP-OPR6279')
{
    dbName = 'tinderAppDB_v2_test'
}

const connectionURL = 'mongodb://127.0.0.1:27017/'+dbName

mongoose.connect(connectionURL).then(()=>{
  console.log("connect to database")
}).catch((e)=>{
  console.log(e)
})
