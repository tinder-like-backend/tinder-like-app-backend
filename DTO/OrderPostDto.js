class OrderPostDto
{
    constructor(buyerid, dto)
    {
        this.buyerid = buyerid;
        this.orderItems = dto.orderItems.map(item => new OrderItem(item.itemid, item.amount, item.fromSharePost=null));
    }
}

class OrderItem
{
    constructor(itemid, amount, fromSharePost)
    {
        this.itemid = itemid;
        this.amount = amount;
        this.fromSharePost = fromSharePost;
    }
}

module.exports = OrderPostDto;