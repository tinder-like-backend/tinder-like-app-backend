
/**
 * 使用到的api:
 *  routers/treemap.js -> /updateByStayingTime
 *
 * @class StayPost
 */
class StayPost{
  constructor(stayTime, buyerid, itemid, labels, feLabels) {
    this.stayTime = stayTime; // unit: ms
    this.buyerid = buyerid;
    this.itemid = itemid;
    this.labels = labels; // e.g. "["美食、伴手禮", "便利/即食品", "熟食小吃"]"
    this.feLabels = feLabels; // e.g. "["美食、伴手禮", "熟食、小吃", "在地小吃、特產"]"
  }
}

module.exports = StayPost;