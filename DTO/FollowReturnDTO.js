class FollowReturnDTO
{
    constructor(followInfo, user)
    {
        this.followInfo = followInfo;
        this.user = user; // buyer or shop
    }
}

module.exports = FollowReturnDTO;