class SharePostLikeDto{
    constructor(buyerid, postid){
        this.buyerid = buyerid;
        this.postid = postid;
    }
}

module.exports = SharePostLikeDto;