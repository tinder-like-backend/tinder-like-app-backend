class GetItemShareDto{
    constructor(itemid, skip, limit){
        this.itemid = itemid;
        this.skip = skip;
        this.limit = limit;
    }
}

module.exports = GetItemShareDto;