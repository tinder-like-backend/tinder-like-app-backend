/**
 *使用到的api:
 *  routers/tinder.js -> /tinderLike、/tinderDisLike、/tinderSuperlike
 * 
 * @class Tinder
 */
class Tinder {
  constructor(buyerid, labels, feLabels, itemid, name) {
    this.buyerid = buyerid;
    this.labels = labels; // e.g. "["美食、伴手禮", "便利/即食品", "熟食小吃"]"
    this.feLabels = feLabels; // e.g. "["美食、伴手禮", "熟食、小吃", "在地小吃、特產"]"
    this.itemid = itemid;
    this.name = name;
  }
}

module.exports = Follow;