class KeywordsGetDto{
    constructor(userId, keywords){
        this.userId = userId;
        this.keywords = keywords.map(keyword => new Keyword(keyword));
    }
}

class Keyword{
    constructor(word, _id, searchDate){
        this.word = word.word;
        this._id = _id;
        this.searchDate = searchDate;
    }
}

module.exports = KeywordsGetDto;