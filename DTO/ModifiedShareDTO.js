class ModifiedShareDTO{
    constructor(postid, name, content){
        this.postid = postid;
        this.name = name;
        this.content = content;
    }
}

module.exports = ModifiedShareDTO;