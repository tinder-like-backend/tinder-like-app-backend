class ShareVideoDto{
    constructor(buyerid, postid, shareVideos){
        this.buyerid = buyerid;
        this.postid = postid;
        this.shareVideos = shareVideos; // this should be an array
    }
}

module.exports = ShareVideoDto;


