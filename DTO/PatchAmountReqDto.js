class PatchAmountReqDto{
    constructor(buyerid, itemid, amount, cost){
        this.buyerid = buyerid;
        this.itemid = itemid;
        this.amount = amount;
    }
}

module.exports = PatchAmountReqDto;