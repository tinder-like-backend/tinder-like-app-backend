class FollowPostDto {
  constructor(followerid, followingid, followType) {
    this.followerid = followerid;
    this.followingid = followingid;
    this.followType = followType; // 0表示賣家追蹤買家，1表示買家追蹤賣家
  }
}

module.exports = FollowPostDto;
