class ShareCommentDto{
    constructor(commentorId, postId, comment, isShowed=1){
        this.commentorId = commentorId;
        this.postId = postId;
        this.comment = comment;
        this.isShowed = isShowed;
    }
}

class ShareCommentLikeDto{
    constructor(commentId, userId){
        this.commentId = commentId;
        this.userId = userId;
    }
}

class ShareCommentReplyDto{
    constructor(commentId, userId, content, isShowed=1){
        this.commentId = commentId;
        this.userId = userId;
        this.content = content;
        this.isShowed = isShowed;
    }
}

module.exports = {
    ShareCommentDto,
    ShareCommentLikeDto,
    ShareCommentReplyDto
};
