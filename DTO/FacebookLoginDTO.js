class FacebookLogInDto
{
    constructor(email, name, profilePic)
    {
        this.email = email;
        this.name = name;
        this.profilePic = profilePic;
    }
}

module.exports = FacebookLogInDto;