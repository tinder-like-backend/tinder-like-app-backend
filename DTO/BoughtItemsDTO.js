class BoughtItem
{
    constructor(item)
    {
        this._id = item._id;
        this.orderid = item.orderid;
        this.name = item.name;
        this.images = item.images;
        this.price = item.price;
        this.amount = item.amount;
        this.payingStatus = item.payingStatus;
        this.shippingStatus = item.shippingStatus;
        this.createdAt = item.createdAt;
    }
}

module.exports = BoughtItem;