class CartDeleteDTO {
  constructor(buyerid, itemid) {
    this.buyerid = buyerid;
    this.itemid = itemid;
  }
}

module.exports = CartDeleteDTO;