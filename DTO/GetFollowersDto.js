class GetFollowersDto{
    constructor(userid, skip, limit){
        this.userid = userid;
        this.skip = skip;
        this.limit = limit;
    }
}

module.exports = GetFollowersDto;