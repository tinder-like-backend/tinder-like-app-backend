class ProfilePicDto{
  constructor(buyerid, fieldname){
    this.buyerid = buyerid;
    this.fieldname = fieldname;
  }
}

module.exports = ProfilePicDto;