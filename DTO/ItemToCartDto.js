class ItemToCartDto{
    constructor(buyerid, itemid, amount, cost, fromSharePost=null){
        this.buyerid = buyerid;
        this.itemid = itemid;
        this.fromSharePost = fromSharePost;
        this.amount = amount;
        this.cost = cost;
    }
}

module.exports = ItemToCartDto;