class OrderReturnDto{
    constructor(orderResult) {
        this.cartid = orderResult._id;
        this.buyerid = orderResult.buyerid;
        this.orderDetails = orderResult.orderDetails.map(item =>{ 
            return new OrderDetails(
                item._doc.item,
                item._doc.amount,
                item._doc.image,
                item._doc.createdAt
            )
        });
    }
}

class OrderDetails{
    constructor(item, amount, image, createdAt) {
        this.item = item;
        this.amount = amount;;
        this.image = image
        this.createdAt = createdAt;
    }
}
module.exports = OrderReturnDto;