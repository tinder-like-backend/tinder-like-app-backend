class SharePostDto {
  constructor(orderid, itemid, buyerid, state = "public", name, content) {
    this.orderid = orderid;
    this.itemid = itemid;
    this.buyerid = buyerid;
    this.state = state;
    this.name=name;
    this.content = content;
  }
}

module.exports = SharePostDto;
