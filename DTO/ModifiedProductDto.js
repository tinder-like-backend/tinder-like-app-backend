class ModifiedProductDto{
    constructor(postid, name, content, labels, feLabels, price, stock, discount){
        this.postid = postid;
        this.name = name;
        this.content = content;
        this.labels = labels;
        this.feLabels = feLabels;
        this.price = price;
        this.stock = stock;
        this.discount = discount;
    }
}

module.exports = ModifiedProductDto;