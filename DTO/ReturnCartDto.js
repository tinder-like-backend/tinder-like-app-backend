class ReturnCartDto{
    constructor(addToCart) {
        this.cartid = addToCart._id;
        this.buyerid = addToCart.buyerid;
        this.cartDetails = addToCart.cartDetails.map(item =>{ 
            return new CartDetails(
                item._doc.item,
                item._doc.amount,
                item._doc.image,
                item._doc.createdAt
            )
        });
    }
}

class CartDetails{
    constructor(item, amount, createdAt) {
        this.item = item;
        this.amount = amount;
        this.createdAt = createdAt;
    }
}

module.exports = ReturnCartDto;