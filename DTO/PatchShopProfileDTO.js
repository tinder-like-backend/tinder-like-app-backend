class PatchShopProfileDTO
{
    constructor(id, name, status, birthday)
    {
        this.id = id;
        this.name = name;
        this.status = status;
        this.birthday = birthday;
    }
}

class PatchShopProfilePicDTO
{
    constructor(shopid, fieldName)
    {
        this.shopid = shopid;
        this.fieldName = fieldName;
    }
}

module.exports = { PatchShopProfileDTO, PatchShopProfilePicDTO };