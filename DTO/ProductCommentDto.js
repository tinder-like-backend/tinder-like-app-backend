class ProductCommentDto{
    constructor(commentorId, itemId, comment, isShowed=1){
        this.commentorId = commentorId;
        this.itemId = itemId;
        this.comment = comment;
        this.isShowed = isShowed;
    }
}

class ProductCommentLikeDto{
    constructor(commentId, userId){
        this.commentId = commentId;
        this.userId = userId;
    }
}

class ProductCommentReplyDto{
    constructor(commentId, userId, content, isShowed=1){
        this.commentId = commentId;
        this.userId = userId;
        this.content = content;
        this.isShowed = isShowed;
    }
}

module.exports = {
    ProductCommentDto,
    ProductCommentLikeDto,
    ProductCommentReplyDto
};