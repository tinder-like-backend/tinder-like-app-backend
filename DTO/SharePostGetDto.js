class SharePostGetDto{
    constructor(buyerid, skip, limit){
        this.buyerid = buyerid;
        this.skip = skip;
        this.limit = limit;
    }
}

module.exports = SharePostGetDto;