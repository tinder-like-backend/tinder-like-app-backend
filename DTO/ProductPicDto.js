class ProductPicDto {
  constructor(shopid, postid, productPics) {
    this.shopid = shopid;
    this.postid = postid;
    this.productPics = productPics; // this should be an array
  }
}

module.exports = ProductPicDto;
