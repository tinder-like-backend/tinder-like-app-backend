class SellerRegisterDTO
{
    constructor(buyerid, shopAccount, selfIntro)
    {
        this.buyerid = buyerid;
        this.shopAccount = shopAccount;
        this.selfIntro = selfIntro;
    }
}  

module.exports = SellerRegisterDTO;