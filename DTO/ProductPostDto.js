// labels 跟feLabels是物件的陣列
// "labels": [
//     {"labelid":1,"display_name":"test1"},
//     {"labelid":1,"display_name":"test2"},
//     {"labelid":1,"display_name":"test3"}
// ]

class ProductPostDto{
    constructor(shopid, shopAccount, name, content, labels, feLabels, price, stock, discount=1){
        this.shopid = shopid;
        this.shopAccount = shopAccount;
        this.name = name;
        this.content = content;
        this.labels = labels;
        this.feLabels = feLabels;
        this.price = price;
        this.stock = stock;
        this.discount = discount;
    }
}

module.exports = ProductPostDto;