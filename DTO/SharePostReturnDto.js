class SharePostReturnDto{
    constructor(postid, buyerid){
        this.postid = postid;
        this.buyerid = buyerid;
    }
}

module.exports = SharePostReturnDto;