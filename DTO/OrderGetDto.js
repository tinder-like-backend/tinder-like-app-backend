class OrderGetDto{
    constructor(buyerid, skip=0, limit=10){
        this.buyerid = buyerid;
        this.skip = skip;
        this.limit = limit;
    }
}

module.exports = OrderGetDto;