class SharePicDto{
    constructor(buyerid, postid, sharePics){
        this.buyerid = buyerid;
        this.postid = postid;
        this.sharePics = sharePics; // this should be an array
    }
}

module.exports = SharePicDto;


