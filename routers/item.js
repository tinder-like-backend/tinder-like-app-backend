const express = require("express");
const ProductPost = require("../models/ShopModel/productPost.js");
const Buyer = require("../models/BuyerModel/buyer");
const SearchHistory = require("../models/ShareModel/searchHistory.js")
const { isAuthenticated, isAuthorized } = require("../auth/auth.js");
const KeywordsGetDto = require("../DTO/KeywordsGetDto.js");

const router = new express.Router();

const findItems = async (condition, limit, skip, sort) => {
  if (condition) {
    const items = await ProductPost.find(condition, null, {
      limit: limit,
      skip: skip,
      sort,
    }).exec();
    return items;
  } else {
    const items = await ProductPost.find({}, null, {
      limit: limit,
      skip: skip,
      sort,
    }).exec();
    return items;
  }
};

router.post("/search", isAuthenticated, async (req, res)=>{
  try{
    let keyword = req.body.keyword;
    let isTry = req.body.isTry;
    
    if(!isTry){
      await SearchHistory.addKeyword(req.payload.id, keyword);
    }
    
    let rgx = new RegExp(keyword, 'i');
    let searchResults = await ProductPost.find(
      {
        name: {$regex: rgx}
      },
      null,
      {
        limit: 100
      }
    );
    return res.status(200).send(searchResults);
  }catch(e){
    console.log(e);
    return res.status(500).send("Internal server error");
  }
});

router.get("/searchKeywords/:limit?/:skip?", isAuthenticated, async (req, res)=>{
    try {
        let keywords;
        if(req.params.limit === undefined){
            keywords = await SearchHistory.getKeywords(req.payload.id)
            return res.status(200).send(keywords)
        }
        // 獲取最新limit筆搜尋紀錄
        keywords = await SearchHistory.getKeywords(req.payload.id, parseInt(req.params.limit), parseInt(req.params.skip))
        return res.status(200).send(new KeywordsGetDto(keywords[0].userId, keywords[0].keywords))
    } catch (error) {
        console.log(error);
        return res.status(500).send("Internal server error");
    }
})

router.delete("/searchKeywords/:keywordId", isAuthenticated, async (req, res)=>{
    try{
        await SearchHistory.removeKeyword(req.payload.id, req.params.keywordId)
        return res.status(200).json({msg: "Delete Success"})
    }catch(error){
        console.log(error);
        return res.status(500).send("Internal server error");
    }
})

router.delete("/searchKeywords", isAuthenticated, async (req, res)=>{
  try{
    await SearchHistory.clear(req.payload.id);
    return res.status(200).json({msg: "Clear Success"});
  }catch(e){
    return res.status(500).send("Internal server error");
  }
});

router.post('/getSearch', async (req, res)=>{
  try{
    let keyword = req.body.keyword;
    let searchResults = await ProductPost.find(
      {
        name: keyword
      },
      null,
      {
        limit: 10
      }
    );
    return res.status(200).send(searchResults);
  }catch(e){
    console.log(e);
    return res.status(500).send("Internal server error");
  }
})

router.get("/random/:num", (req, res) => {
  ProductPost.aggregate([{ $sample: { size: parseInt(req.params.num) } }])
    .then((products) => {
      res.status(200).send(products);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});


// Get productpost by itemid
router.get("/:id", (req, res) => {
  const _id = req.params.id;

  ProductPost.findOne({ _id: _id })
    .then((product) => {
      if (!product) {
        return res.status(404).send("No product");
      }

      res.send(product);
    })
    .catch((e) => {
      res.status(500).send("sever is wrong");
    });
});

router.patch('/like', async (req, res)=>{
  try{
    const {itemid, buyerid} = req.body;
    let buyer = await Buyer.findOne({_id: buyerid});
    if(buyer.likedItems.includes(itemid)){
      return res.status(200).json({msg: "has already liked"});
    }

    let item = await ProductPost.findOne({_id: itemid});
    item.likeCount = item.likeCount+1;
    buyer.likedItems.push(itemid);
    await item.save();
    await buyer.save();
    return res.status(200).send(item);
  }catch(e){
    return res.status(500).json({err: "Internal server error"});
  }
});

router.patch('/unlike', async (req, res)=>{
  try{
    const {itemid, buyerid} = req.body;
    let buyer = await Buyer.findOne({_id: buyerid});
    if(!buyer.likedItems.includes(itemid)){
      return res.status(200).json({msg: "hasn't liked yet"});
    }

    let item = await ProductPost.findOne({_id: itemid});
    item.likeCount = item.likeCount-1;
    buyer.likedItems = buyer.likedItems.filter(x=>x!=itemid);
    await item.save();
    await buyer.save();
    return res.status(200).json({updated: item});
  }catch(e){
    return res.status(500).json({err: "Internal server error"});
  }
});

module.exports = router;