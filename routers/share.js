const express = require("express");
const { isAuthenticated, isAuthorized } = require("../auth/auth.js");
const Buyer = require("../models/BuyerModel/buyer.js");
const Order = require("../models/BuyerModel/order.js");
const LikedSharePost = require("../models/BuyerModel/likedSharePost.js")
const ProductPost = require("../models/ShopModel/productPost.js");
const SharePost = require("../models/BuyerModel/sharePost.js");
const SharePostDto = require("../DTO/SharePostDto.js");
const sharePostPicPath = require("../services/SharePostPicService.js");
const sharePostVideoPath = require("../services/SharePostVideoService.js");
const SharePicDto = require("../DTO/SharePicDto.js");
const SharePostReturnDto = require("../DTO/SharePostReturnDto.js");
const SharePostLikeDto = require("../DTO/SharePostLikeDto.js");
const ShareVideoDto = require("../DTO/ShareVideoDto.js");
const SharePostGetDto = require("../DTO/SharePostGetDto.js");
const GetItemShareDto = require("../DTO/GetItemShareDto.js");
const ModifiedShareDTO = require("../DTO/ModifiedShareDTO.js")
const Response = require("../models/Response");
const router = new express.Router();

// 買家分享商品，要在order裡面有才能分享，會回傳postid跟buyerid用於接下來圖片跟影片的上傳
router.post("/sharePost", isAuthenticated, async (req, res) => {
  try {
    const sharePost = new SharePostDto(
      req.body.orderid,
      req.body.itemid,
      req.body.buyerid,
      req.body.state ?? "public",
      req.body.name,
      req.body.content
    );
 
    // 檢查有無該order
    if(!(await Order.checkOrderOfBuyer(sharePost.orderid))){
      return res.status(400).send(new Response(false, 400, "You haven't bought this item."));
    }

    // 檢查是否曾經PO過
    if ((await SharePost.isPostExist(
        sharePost.buyerid,
        sharePost.orderid,
        sharePost.itemid
      ))
    ) {
      res.status(400).send(new Response(false, 400, "You have posted this Item."));
      return;
    }

    const post = new SharePost(sharePost);
    const savePost = await post.save();

    res.status(201).json(savePost);
    return;
  } catch (e) {
    //console.log(e);
    res.status(500).send(new Response(false, 500, "Internal Server", e));
  }
});

// 圖片最多10張
router.post("/sharePostPics", sharePostPicPath.array("sharePics", 10), async (req, res) => {
    try {
      const shareFiles = new SharePicDto(
        req.body.buyerid,
        req.body.postid,
        req.fileURLs,
      );

      var post = await SharePost.findOne({_id: shareFiles.postid});
      post.images = shareFiles.sharePics ?? [];
      await post.save();
     
      req.fileURLs.length=0; // 清空內存
      res.status(200).json(post);
    } catch (error) {
      return res.status(500).json({ msg: "Internal Server Error" });
    }
  }
);

// 影片最多1個
router.post("/sharePostVideoes", sharePostVideoPath.array("shareVideoes", 1), async (req, res) => {
    try {
      const shareFiles = new ShareVideoDto(
        req.body.buyerid,
        req.body.postid,
        req.videoURLs,
      );

      await SharePost.updateOne(
        { _id: shareFiles.postid },
        {
            videoes: shareFiles.shareVideos,
        }
      );

      const sharePost = await SharePost.findOne({_id: shareFiles.postid}).exec();
      req.videoURLs.length=0; // 清空內存
      res.status(200).json(sharePost);
    } catch (error) {
      return res.status(500).json({ msg: "Internal Server Error" });
    }
  }
);

// 獲取sharepost
router.get("/:buyerid", async (req, res) => {
  try{
    const sharePostGetDto = new SharePostGetDto(
        req.params.buyerid,
        req.query.skip ?? 0,
        req.query.limit ?? 10
    );
    const sharePosts = await SharePost.getSharePosts(sharePostGetDto)
    res.status(200).json(sharePosts);
  } catch (e) {
    console.log(e);
    res.status(500).send("Server Error");
  }
});

// 獲取productPost相關sharePost
router.get("/relatedItem/:itemid", async(req, res) => {
    try{
        const getItemShareDto = new GetItemShareDto(req.params.itemid, req.query.skip ?? 0, req.query.limit ?? 10)
        const sharePosts = await SharePost.getItemSharePosts(getItemShareDto)
        console.log(sharePosts);
        if(!sharePosts){
            res.status(200).json(sharePosts);
        }
        res.status(200).json(sharePosts);
    }
    catch(error){
        console.log(error);
        res.status(500).send("Server Error");
    }
})

// 喜歡sharePost
router.patch('/like', isAuthenticated, async(req, res) => {
    try{
        const sharePostLikeDto = new SharePostLikeDto(req.payload.id, req.body.postid);
        if(await LikedSharePost.ifLikeExist(sharePostLikeDto.buyerid, sharePostLikeDto.postid)){
            return res.status(200).json({msg: "has already liked"});
        }

        let post = await SharePost.getSharePost(sharePostLikeDto.postid);
        post.likeCount += 1;
        const newLike = new LikedSharePost(sharePostLikeDto);
        await newLike.save();
        await post.save();
        return res.status(200).send(post);
    }
    catch(e){
        return res.status(500).json({err: "Internal server error"});
    }
})

router.patch('/unlike', isAuthenticated, async(req, res) => {
    try{
        const sharePostLikeDto = new SharePostLikeDto(req.payload.id, req.body.postid);
        if(!await LikedSharePost.ifLikeExist(sharePostLikeDto.buyerid, sharePostLikeDto.postid)){
            return res.status(200).json({msg: "hasn't liked yet"});
        }

        let post = await SharePost.getSharePost(sharePostLikeDto.postid);
        post.likeCount -= 1;
        await LikedSharePost.deleteOne(sharePostLikeDto).exec();
        await post.save();
        return res.status(200).send(post);
    }
    catch(e){
        return res.status(500).json({err: "Internal server error"});
    }
})

router.get('/like/:postid', isAuthenticated, async(req, res)=>{
  try{
    const { postid } = req.params;
    const likes = await LikedSharePost.find({postid: postid});
    if(!likes){
      return res.status(202).json(new Response(true, 202, "No like in this post.", null));
    }
    return res.status(200).json(new Response(true, 200, "Get likes of this. post", likes));
  }catch(e){
    return res.status(500).json(new Response(false, 500, "Internal Server Error"));
  }
});

router.get('/sharePost/:id', isAuthenticated, async(req, res)=>{
  try{
    const { id } = req.params;
    const post = await SharePost.findOne({_id: id});
    if(!post){
      return res.status(202).json(new Response(true, 202, "No share post found.", null));
    }
    return res.status(200).json(new Response(true, 200, "Get specific share post", post));
  }catch(e){
    return res.status(500).json(new Response(false, 500, "Internal Server Error"));
  }
})

// 刪除sharePost
router.delete('/sharePost/:id', isAuthenticated, async(req, res) => {
    try{
        await SharePost.deleteOne({_id: req.params.id});
        res.status(200).json({msg: "成功刪除貼文"})
    }
    catch(err){
        console.log(err);
        return res.status(500).json(new Response(false, 500, "Internal Server Error"));
    }
})

// 修改sharePost(name、content)
router.patch('/sharePost/:id', isAuthenticated, async(req, res) => {
    try{
        const modifiedShare = new ModifiedShareDTO(req.params.id, req.body.name, req.body.content);
        const modifiedfields = Object.assign({}, modifiedShare);
        delete modifiedfields.postid
        const share = await SharePost.updateOne({_id: modifiedShare.postid}, {...modifiedfields});
        
        const updatedShare = await SharePost.findOne({_id: modifiedShare.postid}).exec();
        res.status(200).json(updatedShare); 
    }
    catch(err){
        console.log(err);
        return res.status(500).json(new Response(false, 500, "Internal Server Error"))
    }
})

module.exports = router;
