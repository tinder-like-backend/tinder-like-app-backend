const express = require("express");
const mongoose = require("mongoose");
const { createSymlink } = require("fs-extra");
const { isAuthenticated, isAuthorized } = require("../auth/auth.js");
const ProductPost = require("../models/ShopModel/productPost.js");
const TreeMapRecommendPool = require("../models/TreeMapModel/treeMapRecommendPool.js");
const Buyer = require("../models/BuyerModel/buyer");
const TinderLike = require("../models/TinderModel/tinderLike");
const TinderUnlike = require("../models/TinderModel/tinderUnlike");
const TinderSuperLike = require("../models/TinderModel/tinderSuperLike");

const router = new express.Router();

/**
 * 測試用數據:
 * 1. labels: ["美妝保養", "眼部保養", "肌膚保養", "眼霜"], feLabels: ["美妝保健", "專櫃清潔保養", "眼霜、眼膜"]
 * 2. labels: ["美食、伴手禮", "便利/即食品", "熟食小吃"], feLabels: ["美食、伴手禮", "熟食、小吃", "在地小吃、特產"]
 */

// tinder like，前端要多傳itemid跟name給我
router.post("/tinderLike", async (req, res) => {
  try {
    // 先對已經在TMRP裡的商品做更新
    await TreeMapRecommendPool.tinderLike(
      req.body.buyerid,
      req.body.labels,
      req.body.feLabels,
      5,
      10
    );

    // 找出被update的items
    const updatedProducts = await TreeMapRecommendPool.find({buyerid: req.body.buyerid, "labels.display_name": {$in: req.body.labels}}, 'itemid')

    // 將已經在TMRP的items放到array裡
    existItems = []
    updatedProducts.forEach(async p => {
      existItems.push(new mongoose.Types.ObjectId(p.itemid))
    })
    
    // 接下來執行ProductPost的更新，如果已經有在existItems中就不要更新，因為已經更新過了
    const recommendProducts = await ProductPost.tinderLike(
      req.body.buyerid,
      req.body.labels,
      req.body.feLabels,
      existItems,
      5,
      10,
      100
    );

    await TreeMapRecommendPool.insertMany(recommendProducts)
    const likeItem = new TinderLike(req.body);
    await likeItem.save();

    res.status(200).json({msg: "tinderLike done."})

  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
});

// tinder super like. Remember to deduct the residual times of superLike by one
router.post("/superLike", async (req, res) => {
  try {
    //先檢查還有沒有superlike的次數
    const buyer = await Buyer.findOne({_id: req.body.buyerid})
    const superLikeLeft = buyer.superLikeLeft
    // console.log(superLikeLeft)

    if(superLikeLeft > 0){
      // 先對已經在TMRP裡的商品做更新
      await TreeMapRecommendPool.tinderLike(
        req.body.buyerid,
        req.body.labels,
        req.body.feLabels,
        10,
        30
      );
  
      // 找出被update的items
      const updatedProducts = await TreeMapRecommendPool.find({buyerid: req.body.buyerid, "labels.display_name": {$in: req.body.labels}}, 'itemid')
  
      // 將已經在TMRP的items放到array裡
      existItems = []
      updatedProducts.forEach(async p => {
        existItems.push(new mongoose.Types.ObjectId(p.itemid))
      })
      
      // 接下來執行ProductPost的更新，如果已經有在existItems中就不要更新，因為已經更新過了
      const recommendProducts = await ProductPost.tinderLike(
        req.body.buyerid,
        req.body.labels,
        req.body.feLabels,
        existItems,
        10,
        30,
        300
      );
  
      await TreeMapRecommendPool.insertMany(recommendProducts)
      const superlikeItem = new TinderSuperLike(req.body);
      await superlikeItem.save();
  
      // 扣除 superlike次數
      await Buyer.updateOne({_id: req.body.buyerid}, {"$inc": {"superLikeLeft": -1}})
  
      res.status(200).json({msg: "superlike done."})
    }
    else{ // 已經用完superLike了
      res.status(403).json({msg: "No superlike left."})
    }

  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
})

// tinder dislike: 直接對TreemapRecommendPool更新，使用同樣的function，只是改成負數
router.patch('/tinderDislike', async (req, res) => {
  try {
    const updatedResponse = await TreeMapRecommendPool.tinderLike(
      req.body.buyerid,
      req.body.labels,
      req.body.feLabels,
      -5,
      -10
    );

    // 從TreemapRecommendPool剔除分數小於0的items
    const lessThanZero = await TreeMapRecommendPool.deleteMany({score: {$lt: 0}})
    // console.log(lessThanZero)

    const unlikeItem = new TinderUnlike(req.body);
    await unlikeItem.save()

    res.status(200).json({updated: updatedResponse.modifiedCount, deleted: lessThanZero.deletedCount})
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
})

// switch page from tinder page，每次換頁就清理一下該使用者的recommendPool，items的數量要少於500個並且大於100個
router.put('/switchPageFromTinder', async (req, res) => {
  try {
    // 計算該使用者目前有多少在TMRP的items並依經過hours更新分數
    const updateInfo = await TreeMapRecommendPool.countItemsAndUpdateByTime(req.body.buyerid, -1, "hour")
    const count = updateInfo.matchedCount

    if(count > 500){ // TMRP 大於500就刪掉多的
      const delItems = await TreeMapRecommendPool.getDelItems(req.body.buyerid, count-500)
      delItems.forEach(async (item) =>{
        await TreeMapRecommendPool.deleteOne(item)
      })

      res.status(200).json({msg: "items more than 500"})
    }
    else if(count < 100){ // 之後再想要怎麼辦，直接捕到500個
      res.status(200).json({msg: "items less than 100"})
    } 
    else{
      res.status(200).json({msg: "items >= 100 and items <= 500"})
    }

  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
})

module.exports = router;
