require('dotenv').config();
const express = require("express");
const { isAuthenticated, isAuthorized, authorizedFor, accountRole } = require("../auth/auth.js");
const Buyer = require("../models/BuyerModel/buyer.js");
const Order = require("../models/BuyerModel/order.js");
const ProductPost = require("../models/ShopModel/productPost.js");
const SharePost = require("../models/BuyerModel/sharePost.js");
const Follow = require("../models/BuyerModel/follow.js");
const Shop = require("../models/ShopModel/shop.js");
const Response = require("../models/Response");
const profilePicPath = require("../services/ProfilePicService")
const ProfilePicDto = require("../DTO/ProfilePicDto")

const router = new express.Router();


router.get("/", isAuthenticated, authorizedFor(accountRole.buyer), isAuthorized, (req, res) => {
  res.send("test");
});

router.get("/buyer/:name", async (req, res) => {
  try{
    let name = req.params.name;
    let rgx = new RegExp(name, 'i');
    let buyers = await Buyer.find(
      {
        account: {$regex: rgx}
      },
      null,
      null
    );
    return res.status(200).json(buyers);
  }catch(err){
    return res.status(500).send("Internal server error");
  }
});

router.get("/buyer", async (req, res)=>{
  try{
    let buyerid = req.query.buyerid;
    let buyer = await Buyer.findOne({_id: buyerid});
    if(buyer){
      return res.status(200).send(buyer);
    }else{
      return res.status(200).send(new Response(false, 200, "no buyer"));
    }
    
  }catch(e){
    console.log(e);
    return res.status(500).send("Internal Server Error.")
  }
});

router.get("/getid", isAuthenticated, authorizedFor(accountRole.buyer), isAuthorized, async(req, res)=>{
  try{
    let email = req.payload.email;
    const buyer = await Buyer.findOne({email: email});
    return res.status(200).json(new Response(true, 200, "Get buyer id", buyer._id));
  }catch(e){
    return res.status(500).json(new Response(false, 500, "Internal Server Error"));
  }
});

router.patch("/profile", async(req, res)=>{
  try{
    let {_id, name, status, birthday} = req.body;
    let buyer = await Buyer.findOneAndUpdate({_id: _id}, {
      account: name,
      selfIntro: status,
      birthday: birthday
    }, {new: true});
    console.log(buyer);
    return res.status(200).json(new Response(true, 200, "patch buyer successfully", buyer));
  }catch(e){
    return res.status(500).json({msg: "Internal Server Error"})
  }  
});

router.post("/profilePic", profilePicPath.single('profilePic') ,async(req, res)=>{
  try {
    const profilePic = new ProfilePicDto(req.body.buyerid, req.file.fieldname)
    await Buyer.updateOne({_id: profilePic.buyerid}, {
      profilePic: req.fileURL
    })
    res.status(200).json({msg: "Profile picture created."})
  } catch (error) {
    return res.status(500).json({msg: "Internal Server Error"})
  }
})

router.patch("/profilePic", profilePicPath.single('profilePic') ,async(req, res)=>{
  try {
    const profilePic = new ProfilePicDto(req.body.buyerid, req.file.fieldname)
    await Buyer.updateOne({_id: profilePic.buyerid}, {
      profilePic: req.fileURL
    })
    res.status(200).json({msg: "Profile picture updated."})
  } catch (error) {
    return res.status(500).json({msg: "Internal Server Error"})
  }
});


module.exports = router;

