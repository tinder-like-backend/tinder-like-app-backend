const express = require("express");
const ProductPostComment = require("../models/ShopModel/productPostComment.js");
const SharePostComment = require("../models/BuyerModel/sharePostComment.js");
const {
  ProductCommentDto,
  ProductCommentLikeDto,
  ProductCommentReplyDto
} = require("../DTO/ProductCommentDto.js");
const {
    ShareCommentDto,
    ShareCommentLikeDto,
    ShareCommentReplyDto
} = require("../DTO/ShareCommentDto.js");
const Buyer = require("../models/BuyerModel/buyer.js");
const Shop = require("../models/ShopModel/shop.js");
const ProductPost = require("../models/ShopModel/productPost.js");
const SharePost = require("../models/BuyerModel/sharePost.js");
const ProductPostCommentLike = require("../models/ShopModel/productPostCommentLike.js");
const ProductPostCommentReply = require("../models/ShopModel/productPostCommentReply.js");
const { isAuthenticated } = require("../auth/auth.js");
const SharePostCommentLike = require("../models/BuyerModel/sharepostCommentLike.js");
const SharePostCommentReply = require("../models/BuyerModel/sharePostCommentReply.js");
const router = new express.Router();

router.post("/product", async (req, res) => {
  try {
    const body = req.body;
    const comment = new ProductCommentDto(
      body.commentorId,
      body.itemId,
      body.comment,
      body.isShowed ? body.isShowed : 1
    );

    if (
      (await Buyer.isBuyerExist(body.commentorId)) &&
      ProductPost.isItemExist(body.itemId)
    ) {
      newComment = new ProductPostComment(comment);
      await newComment.save();
      res.status(201).json(comment);
    } else {
      res.status(404).json({ error: "Buyer Not Found" });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.get("/product/:itemId", isAuthenticated, async (req, res) => {
  try {
    let buyerid = req.payload.id;
    const comments = await ProductPostComment.getCommentandUserDetail(
      req.params.itemId,
      req.query.limit === undefined ? req.query.limit : 10,
      req.query.skip === undefined ? req.query.skip : 10
    );
    for (comment of comments) {
      let commentor = await Shop.getAccountAndPic(comment.commentorId);
      if (!commentor) {
        commentor = await Buyer.getAccountAndPic(comment.commentorId);
      }
      comment._doc["profilePic"] = commentor.profilePic;
      comment._doc["account"] = commentor.account;
      comment._doc["isLiked"] = await ProductPostCommentLike.isLikeExist(
        comment._id,
        buyerid
      );
    }
  
    res.status(200).json({ comments: comments });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.get("comment/:commentId", async (req, res) => {
  try {
    let commentId = req.params.commentId;
    let comment = await ProductPostComment.findOne({ _id: commentId });
    return res.status(200).json(comment);
  } catch (e) {
    return res.status(500).send("Internal server error");
  }
});

// 第一次post是like，第二次post是取消like
router.post("/product/like", async (req, res) => {
  try {
    const productCommentLike = new ProductCommentLikeDto(
      req.body.commentId,
      req.body.userId
    );

    let comment = await ProductPostComment.findOne({ _id: productCommentLike.commentId });

    // check if the user is already like the comment => remove like if exist
    if (
      await ProductPostCommentLike.isLikeExist(
        productCommentLike.commentId,
        productCommentLike.userId
      )
    ) {
      await ProductPostCommentLike.removeLike(
        productCommentLike.commentId,
        productCommentLike.userId
      );

      comment.likeCount = comment.likeCount-1;
      await comment.save();

      return res.status(200).json({ msg: "You have removed like." });
    }

    const result = await ProductPostCommentLike.addLike(
      productCommentLike.commentId,
      productCommentLike.userId
    );
    comment.likeCount = comment.likeCount + 1;
    await comment.save();

    return res.status(200).send(result);

  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.post("/product/reply", async (req, res) => {
  try {
    const reply = new ProductCommentReplyDto(
      req.body.commentId,
      req.body.userId,
      req.body.content,
      req.body.isShowed ?? 1
    );

    const result = await ProductPostCommentReply.addAndGetReply(reply);
    res.status(200).send(result);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.post("/sharePost", async (req, res) => {
  try {
    const body = req.body;
    const comment = new ShareCommentDto(
      body.commentorId,
      body.postId,
      body.comment,
      body.isShowed ? body.isShowed : 1
    );
    console.log(comment);
    if (
      (await Buyer.isBuyerExist(body.commentorId)) &&
      SharePost.isPostExist(body.postId)
    ) {
      newComment = new SharePostComment(comment);
      await newComment.save();
      res.status(201).json(comment);
    } else {
      res.status(404).json({ error: "Buyer Not Found" });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.get("/sharePost/:postId", isAuthenticated, async (req, res) => {
  try {
    const comments = await SharePostComment.getCommentandUserDetail(
      req.params.postId,
      req.query.limit === undefined ? req.query.limit : 10,
      req.query.skip === undefined ? req.query.skip : 0
    );
    for (comment of comments) {
      let commentor = await Shop.getAccountAndPic(comment.commentorId);
      if (!commentor) {
        commentor = await Buyer.getAccountAndPic(comment.commentorId);
      }
      comment._doc["profilePic"] = commentor.profilePic;
      comment._doc["account"] = commentor.account;
      comment._doc["isLiked"] = await SharePostCommentLike.isLikeExist(
        comment._id,
        req.payload.id
      )
    }

    res.status(200).json({ comments: comments });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.post("/sharePost/like", async (req, res) => {
  try {
    const sharePostCommentLike = new ShareCommentLikeDto(
      req.body.commentId,
      req.body.userId
    );
    // console.log(sharePostCommentLike)
    let comment = await SharePostComment.findOne({ _id: sharePostCommentLike.commentId });
    // console.log(comment);

    // check if the user is already like the comment => remove like if exist
    if (
      await SharePostCommentLike.isLikeExist(
        sharePostCommentLike.commentId,
        sharePostCommentLike.userId
      )
    ) {
      await SharePostCommentLike.removeLike(
        sharePostCommentLike.commentId,
        sharePostCommentLike.userId
      );

      comment.likeCount = comment.likeCount - 1;
    
      await comment.save();  
      res.status(200).json({ msg: "You have removed like." });
      return;
    }
    const result = await SharePostCommentLike.addLike(
        sharePostCommentLike.commentId,
        sharePostCommentLike.userId
    );
    comment.likeCount = comment.likeCount + 1;
    await comment.save();
    res.status(200).send(result);
    return;
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.post("/sharePost/reply", async(req, res) => {
    try{
        const reply = new ShareCommentReplyDto(
            req.body.commentId,
            req.body.userId,
            req.body.content,
            req.body.isShowed ?? 1
        );
    
        const result = await SharePostCommentReply.addAndGetReply(reply);
        res.status(200).send(result);
    }
    catch(error){
        console.log(error);
    }
})

module.exports = router;
