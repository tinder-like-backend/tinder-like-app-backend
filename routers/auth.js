const Buyer = require("../models/BuyerModel/buyer");
const jwt = require("jsonwebtoken");
const config = require("../config/config.json");
const { isAuthenticated, accountRole } = require("../auth/auth.js");
const express = require("express");
const router = new express.Router();
const auth = require('../auth/auth');
const Response = require('../models/Response');
const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcrypt');
const moment = require('moment');
const SendEmail = require('../services/send-email');
const { access } = require("fs-extra");
const FacebookLogInDto = require("../DTO/FacebookLoginDTO");
const Shop = require('../models/ShopModel/shop');
const SellerRegisterDTO = require('../DTO/SellerRegisterDTO');

router.post("/login", async (req, res) => {
  let { email, password } = req.body;

  //check from database
  try {
    //check is crypted
    let isHaveSalt = false;
    let unCheckedBuyer = await Buyer.findOne({email: email});
    isHaveSalt = unCheckedBuyer.salt?true:false;
  
    password = isHaveSalt?bcrypt.hashSync(password, unCheckedBuyer.salt):password;

    const buyer = await Buyer.findOne({ email: email, password: password });
    if(!buyer){
      return res.status(400).json(new Response(false, 400, "No user found"));
    }

    let isHasActive = buyer.active?true:false; 

    if (buyer && (isHasActive&&buyer.active || !isHasActive)) { 
      const accessToken = jwt.sign(
        {
          email: email,
          id: buyer._id,
          role: "buyer",
        },
        config.accessScret,
        {
          expiresIn: "24h",
        }
      );

      //不用超時重新登入->uuid, 有要超時登入->JWT
      const refreshToken = jwt.sign(
        {
          email: email,
          id: buyer._id,
          role: "buyer",
        },
        config.refreshSecret,
        {
          expiresIn: "7d",
        }
      );

      return res
        .status(200)
        .json({ accessToken: accessToken, refreshToken: refreshToken });
    } else {
      return res.status(400).json({ success: false, error: "No user found" });
    }
  } catch (e) {
    return res
      .status(500)
      .json({ success: false, error: "Internal Server Error" });
  }
});

router.post("/logout", isAuthenticated, async (req, res) => {
  //cancel the refresh token in database
});

router.post('/register', async (req, res)=>{
  try {
    const check = await Buyer.findOne({email: req.body.email});
    if(check){
      return res.status(202).json(new Response(false, 202, "The user has been registered."));
    }

    let buyerSchema = req.body; //has only email,password,account,gender
    let { email, password, account, gender} = req.body;
    
    //prepare filed
    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);
    var hash = bcrypt.hashSync(password, salt);
    password = hash;

    let token = uuidv4();
    let expire = moment().add(30, 'minutes').format("YYYY-MM-DD HH:mm");
    
    //filter info
    buyerSchema.salt = salt;
    buyerSchema.password = password;
    buyerSchema.verify_token = token;
    buyerSchema.verify_expire = expire;
    buyerSchema.active = false;
    buyerSchema.gender = gender;

    //initial info
    buyerSchema.role = "buyer";
    buyerSchema.awardCoin = 0;
    buyerSchema.profilePic = "http://140.119.19.32/images/user.jpg"; //預設圖片
    buyerSchema.selfIntro = "";
    buyerSchema.public = true;
    buyerSchema.followerCount = 0;
    buyerSchema.followingCount = 0;
    buyerSchema.superLikeLeft = 0;
    buyerSchema.likedItems = [];
    
    //insert
    let buyer = new Buyer(buyerSchema);
    await buyer.save();

    //find
    buyer = await Buyer.findOne({email: email, password: password});
    let buyerId = buyer.id;

    //send email
    let se = new SendEmail(email, config.projectEmail, "<<CC: Verification Your Account>>", `
        Hello! ${account}
        Please click the link: ${config.host}/verification?id=${buyerId}&token=${token} to activate your account.
        Thanks for using our app!
    `);
    let re = se.sendEmail();

    return res.status(201).json(new Response(true, 201, "Register successfully, check the email", re));
  } catch (e) {
    console.log(e);
    return res.status(500).json(new Response(false, 500, "Internal server error"));
  }
});

router.get('/verification', async (req, res)=>{
  let buyerId = req.query.id;
  let token = req.query.token;

  let buyer = Buyer.findOne({_id: buyerId, token: token});
  let isExpired = moment().format("YYYY-MM-DD HH:mm")<buyer.expire;

  if(isExpired){
    //delete that user
    await buyer.remove().exec();
    res.send("The verification link is expired. Please register again.");
  }

  await Buyer.findOneAndUpdate({_id: buyerId}, {
    active: true
  });
 
  res.send("Activate account successfully! Try to Login!");
});

router.post('/refresh', async (req, res)=>{
  const {email, refreshToken, id} = req.body;
  let isValid = auth.verifyRefresh(email, refreshToken);
  if(!isValid){
    return res.status(401).send("Invalid token, try login again");
  }
  var role = auth.getRoleFromRefreshToken(refreshToken);
  if(!role){
    return res.status(401).send("Invalid token, try login again");
  }

  accessToken = jwt.sign(      
    {
      email: email,
      id: id,
      role: role,
    },
    config.accessScret,
    {
      expiresIn: "24h",
    }
  );

  return res.status(200).json(accessToken);
});

router.post('/login/facebook', async (req, res)=>{
  try
  {
    const dto = new FacebookLogInDto(
      email=req.body.email,
      name=req.body.name,
      profilePic=req.body.profilePic
    );

    const check = await Buyer.findOne({email: dto.email});
    if(check){
      return res.status(202).json(new Response(false, 202, "The user has been registered."));
    }
  
    //no password, gender
    var newBuyer={};
  
    //init info
    newBuyer.profilePic = dto.profilePic;
    newBuyer.email = dto.email;
    newBuyer.account = dto.name;
    newBuyer.active = true;
    newBuyer.gender = ""; //unset gender
    newBuyer.role = "buyer";
    newBuyer.awardCoin = 0;
    newBuyer.selfIntro = "";
    newBuyer.public = true;
    newBuyer.followerCount = 0;
    newBuyer.followingCount = 0;
    newBuyer.superLikeLeft = 0;
    newBuyer.likedItems = [];

    //insert
    const buyer = new Buyer(newBuyer);
    await buyer.save();

    //give token
    const accessToken = jwt.sign(
      {
        email: buyer.email,
        id: buyer._id,
        role: "buyer",
      },
      config.accessScret,
      {
        expiresIn: "24h",
      }
    );

    const refreshToken = jwt.sign(
      {
        email: buyer.email,
        id: buyer._id,
        role: "buyer",
      },
      config.refreshSecret,
      {
        expiresIn: "7d",
      }
    );
    
    return res.status(200).json(new Respinse(true, 200, "facebook login successfully", { accessToken: accessToken, refreshToken: refreshToken }));
  }catch(err){
    return res.status(500).json("Internal Server Error");
  }
});

//重設密碼驗證信
router.post('/reset/password/email', async (req, res)=>{
  try{
    const { email } = req.body;
    var buyer = await Buyer.findOne({email: email});
    
    if(!buyer){
      return res.status(202).json(new Response(false, 202, "No such user."));
    }

    // for verify, if already exist, the cover it
    const code = Math.floor(Math.random()*9000+1000);
    buyer.resetToken = code;
    buyer.resetTokenExpire = moment().add(30, 'minutes').format("YYYY-MM-DD HH:mm");
    await buyer.save();

    var sendEmailService = new SendEmail(email, config.projectEmail, "<<CC: Reset Password>>", `
      Hello! This is the reset-password-email from CC.
      Your verification code is ${code}
    `);
    var re = sendEmailService.sendEmail();

    return res.status(200).json(new Response(true, 200, "Verification code email has been sent.", re));
  }catch(err){
    return res.status(500).json("Internal Server Error")
  }
});

router.get('/verification/resetPasswordCode', async (req, res)=>{
  try{
    const { code, email } = req.query;
    var buyer = await Buyer.findOne({email: email});

    const isMatch = code === buyer.resetToken;
    if(!isMatch){
      return res.status(200).json(new Response(false, 202, "Verification code is unmatched."));
    }
    const isExpired = moment().format("YYYY-MM-DD HH:mm") > buyer.resetTokenExpire;
    if(isExpired){
      return res.status(200).json(new Response(false, 202, "Verification code has expired."));
    }

    return res.status(200).json(new Response(true, 200, "Verify successfully!", code));
  }catch(err){
    return res.status(500).json("Internal Server Error")
  }
});

//重設密碼
router.patch('/reset/password', async (req, res)=>{
  try{
    const { newPassword, code, email } = req.body;
    var buyer = await Buyer.findOne({email: email});

    //guard
    const isMatch = code === buyer.resetToken;
    if(!isMatch){
      return res.status(200).json(new Response(false, 202, "Verification code is unmatched."));
    }
    const isExpired = moment().format("YYYY-MM-DD HH:mm") > buyer.resetTokenExpire;
    if(isExpired){
      //verify code, if expire then delete
      buyer.resetToken = undefined;
      buyer.resetTokenExpire = undefined;
      await buyer.save();
      return res.status(200).json(new Response(false, 202, "Verification code has expired. Please send the email again."));
    }

    //reset password and salt
    var password = newPassword;
    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);
    var hash = bcrypt.hashSync(password, salt);
    password = hash;

    buyer.salt = salt;
    buyer.password = password;

    await buyer.save();

    return res.status(200).json(new Response(true, 200, "Reset password successfully!"))
  }catch(err){
    return res.status(500).json("Internal Server Error")
  }
});

router.get('/isRegisteredSeller/:buyerid', isAuthenticated, async (req, res)=>{
  try{
    const buyerid = req.params.buyerid;
    const tryShop = await Shop.findOne({buyerid: buyerid});
    if(tryShop){
      return res.status(200).json(new Response(true, 200, "This buyer has registered a seller.", {isRegistered: true}));
    }else{
      return res.status(200).json(new Response(true, 200, "This buyer hasn't registered a seller.", {isRegistered: false}));
    }
    
  }catch(err){
    return res.status(500).json({err: err});
  }
});

router.post("/seller-register", isAuthenticated, async (req, res) => {
  try{
    const dto = new SellerRegisterDTO(
      buyerid = req.payload.id,
      shopAccount = req.body.shopAccount,
      selfIntro = req.body.selfIntro
    );

    // check has the shop account been taken
    const check = await Shop.findOne({shopAccount: dto.shopAccount});
    if(check){
      return res.status(202).json(new Response(false, 202, "This shop name has been taken"));
    }

    const registerBuyer = await Buyer.findOne({_id: dto.buyerid});
    var shopSchema = {};
    //init
    shopSchema.buyerid = dto.buyerid;
    shopSchema.shopAccount = dto.shopAccount;
    shopSchema.selfIntro = dto.selfIntro;
    shopSchema.role = accountRole.shop;
    shopSchema.email = registerBuyer.email;
    shopSchema.itemcount = 0;
    shopSchema.profilePic = registerBuyer.profilePic;
    shopSchema.rating = {
      ratingStar: 0,
      ratingBad: 0,
      ratingNormal: 0,
      ratingGood: 0
    }
    shopSchema.likeCount = 0;
    shopSchema.public = true;
    shopSchema.followerCount = 0;
    shopSchema.followingCount = 0;
  
    var shop = new Shop(shopSchema);
    await shop.save();

    return res.status(200).json(new Response(true, 200, "Register seller successfully!"))

  }catch(err){
    return res.status(500).json({err: err});
  }
});

router.post("/switch-account", isAuthenticated, async (req, res)=>{
  let payload = req.payload; //after middleware
  let fromType = payload.role; //original
  let toType = req.body.to;
  let check = fromType in auth.accountRole && toType in auth.accountRole;
  let id = payload.id;

  if(!check){
    return res.status(400).json(new Response(false, 400, "Invalid role request"));
  }

  let originalUser = null;
  if(fromType===auth.accountRole.buyer){
    originalUser = await Buyer.findOne({_id: id});
  }else if(fromType===auth.accountRole.shop){
    originalUser = await Shop.findOne({_id: id});
  }
  
  let toUser = null;
  if(toType===auth.accountRole.buyer){
    // shop to buyer
    toUser = await Buyer.findOne({_id: originalUser.buyerid});
  }else if(toType===auth.accountRole.shop){
    // buyer to shop
    toUser = await Shop.findOne({buyerid: originalUser._id});
  }

  if(toUser==null){
    return res.status(202).json(new Response(true, 202, "No to user found.\nCheck if has registered."));
  }

  const newAccessToken = jwt.sign(
    {
      email: payload.email,
      id: toUser._id,
      role: toType,
    },
    config.accessScret,
    {
      expiresIn: "24h",
    }
  );

  const newRefreshToken = jwt.sign(
    {
      email: payload.email,
      id: toUser._id,
      role: toType,
    },
    config.refreshSecret,
    {
      expiresIn: "7d",
    }
  );

  return res.status(200).json(new Response(
    true, 
    200, 
    `switch account from ${fromType} to ${toType}`,
    {
      email: toUser.email,
      newAccessToken: newAccessToken, 
      newRefreshToken: newRefreshToken
    }
   )
  );
});

router.get('/tokenRole', isAuthenticated, async(req, res)=>{
  try{
    const role = req.payload.role;
    return res.status(200).json(new Response(true, 200, "Role based on token", {role: role}));
  }catch(err){
    return res.status(500).json(err);
  }
});

module.exports = router;
