const express = require("express");
const mongoose = require("mongoose");
const { createSymlink } = require("fs-extra");
const { isAuthenticated, isAuthorized } = require("../auth/auth.js");
const ProductPost = require("../models/ShopModel/productPost.js");
const TreeMapRecommendPool = require("../models/TreeMapModel/treeMapRecommendPool.js");
const StayPost = require("../DTO/StayPost");

const router = new express.Router();

router.get("/recommendScore/:buyerid/:itemid", async(req, res)=>{
  try{
    const buyerid = req.params.buyerid;
    const itemid = req.params.itemid;
    let rec = await TreeMapRecommendPool.findOne({buyerid: buyerid, itemid: itemid});
    return res.status(200).json(rec);
  }catch(e){
    return res.status(500).json(new Response(false, 500, "Internal Server Error"));
  }
});

// TreeMap顯示商品，有顯示到treeMap的商品要扣分
router.get("/recommendItems/:buyerid", async (req, res) => {
  try {
    const itemids = await TreeMapRecommendPool.getTreemapItems(
      req.params.buyerid,
      60
    );
    console.log(itemids);
    let ids = [];
    itemids.forEach((itemid) => {
      ids.push(itemid.itemid);
    });

    // 有出現在treeMapPool就扣分(-5)
    await TreeMapRecommendPool.updateWhenShowOnTreeMap(
      req.params.buyerid,
      ids,
      -5
    );

    const items = await ProductPost.find({ _id: { $in: ids } }).exec();

    res.status(200).send(items);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
});

// 透過停留時間更新分數, 跟tinderLike操作差不多
router.patch("/updateByStayingTime", async (req, res) => {
  try {
    const stayPost = new StayPost(
      req.body.stayTime,
      req.body.buyerid,
      req.body.itemid,
      req.body.labels,
      req.body.feLabels
    );
    console.log(stayPost);
    // 先對已經在TMRP裡的商品做更新
    await TreeMapRecommendPool.updateByStayingTimeOnPost(
      stayPost.buyerid,
      stayPost.labels,
      stayPost.feLabels,
      1 * 0.5 * stayPost.stayTime * 0.00005,
      2 * 0.5 * stayPost.stayTime * 0.00005
    );

    // 找出被update的items
    const updatedProducts = await TreeMapRecommendPool.find(
      {
        buyerid: req.body.buyerid,
        "labels.display_name": { $in: req.body.labels },
      },
      "itemid"
    );

    // 將已經在TMRP的items放到array裡
    existItems = [];
    updatedProducts.forEach(async (p) => {
      existItems.push(new mongoose.Types.ObjectId(p.itemid));
    });

    // 接下來執行ProductPost的更新，如果已經有在existItems中就不要更新，因為已經更新過了
    const recommendProducts = await ProductPost.tinderLike(
      req.body.buyerid,
      req.body.labels,
      req.body.feLabels,
      existItems,
      1 * 0.5 * stayPost.stayTime * 0.00001,
      3 * 0.5 * stayPost.stayTime * 0.00001,
      100
    );

    await TreeMapRecommendPool.insertMany(recommendProducts);

    res.status(200).json({ msg: "Updated." });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: error.message });
  }
});

module.exports = router;
