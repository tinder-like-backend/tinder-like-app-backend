require('dotenv').config();
const express = require("express");
const { isAuthenticated, isAuthorized, authorizedFor, accountRole } = require("../auth/auth.js");
const Buyer = require("../models/BuyerModel/buyer.js");
const FollowPostDto = require("../DTO/FollowPostDto.js")
const Follow = require("../models/BuyerModel/follow.js");
const Shop = require("../models/ShopModel/shop.js");
const GetFollowingsDto = require("../DTO/GetFollowingsDto.js")
const GetFollowersDto = require("../DTO/GetFollowersDto.js")

const router = new express.Router();

// 買家追蹤功能
router.post("/", isAuthenticated, authorizedFor(accountRole.buyer, accountRole.shop), isAuthorized,
 async (req, res) => {
    try {
      const followPostDto = new FollowPostDto(req.payload.id, req.body.followingid, req.body.followType) // followType: 0 => 表示買家追蹤買家， 1 => 表示買家追蹤賣家

      if(await Follow.isFollowExist(followPostDto.followerid, followPostDto.followingid)){
        return res.status(403).json({msg: "Already followed"});
      }else{
        const follow = new Follow(followPostDto);
        let follower = await Follow.findFollower(followPostDto.followerid, req.payload.role);
        let following = await Follow.findFollowing(followPostDto.followingid, followPostDto.followType);
    
        follower.followingCount += 1;
        following.followerCount += 1;

        await follower.save();
        await following.save();
        await follow.save()
        
        // 回傳被追蹤者，可以修改following followerCount
        res.status(200).json(following)
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error.message });
    }
})
  
// 取消追蹤功能
router.delete("/:followType/:followerid/:followingid", 
isAuthenticated, authorizedFor(accountRole.buyer, accountRole.shop), isAuthorized,
async (req, res) => {
    try {
        const followPostDto = new FollowPostDto(req.params.followerid, req.params.followingid, parseInt(req.params.followType))
        console.log(followPostDto)
        // 要先檢查是否有追蹤過了
        if(await Follow.isFollowExist(followPostDto.followerid, followPostDto.followingid)){
            let follower = await Follow.findFollower(followPostDto.followerid, req.payload.role);
            let following = await Follow.findFollowing(followPostDto.followingid, followPostDto.followType);
            await Follow.removeFollow(followPostDto.followerid, followPostDto.followingid)

            follower.followingCount -= 1;
            following.followerCount -= 1;
            
            await follower.save();
            await following.save();

            // 回傳被追蹤者，可以修改following followerCount
            return res.status(200).json(following)
        }else{
            res.status(403).json({msg: "Not yet Followed"});
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: error.message });
    }
})

router.get("/followings/:userid/:skip/:limit", isAuthenticated, async (req, res) => {
    try{
        const getFollowingsReq = new GetFollowingsDto(req.params.userid, req.params.skip, req.params.limit);
        //may be this user or other user
        const followings = await Follow.getFollowings(getFollowingsReq.userid, getFollowingsReq.skip ?? 0, getFollowingsReq.limit ?? 50);
        return res.status(200).json(followings); // DTO
    }catch (error) {
        console.log(error);
        return res.status(500).json({ error: error.message });
    }
})

router.get("/followers/:userid/:skip/:limit", isAuthenticated, async (req, res) => {
    try{
        const getFollowersReq = new GetFollowersDto(req.params.userid, req.params.skip, req.params.limit);
        const followers = await Follow.getFollowers(getFollowersReq.userid, getFollowersReq.skip ?? 0, getFollowersReq.limit ?? 50);
        return res.status(200).json(followers); //DTO
    }catch (error) {
        console.log(error);
        return res.status(500).json({ error: error.message });
    }
})

router.get("/isFollow/:followerid/:followingid", isAuthenticated, async (req, res)=>{
    try{
        var isFollow = await Follow.isFollowExist(req.params.followerid, req.params.followingid);
        return res.status(200).json(isFollow);
    }catch(err){
        return res.status(500).json({ error: error.message });
    }
});

module.exports = router;