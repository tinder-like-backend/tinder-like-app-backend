const express = require("express");
const { isAuthenticated, isAuthorized } = require("../auth/auth.js");
const Buyer = require("../models/BuyerModel/buyer.js");
const Order = require("../models/BuyerModel/order.js");
const OrderPostDto = require("../DTO/OrderPostDto.js");
const OrderGetDto = require("../DTO/OrderGetDto.js");
const OrderReturnDto = require("../DTO/OrderReturnDto.js");
const ProductPost = require("../models/ShopModel/productPost.js");
const ShopCart = require("../models/BuyerModel/shopCart.js");
const BoughtItem = require("../DTO/BoughtItemsDTO.js");

const router = new express.Router();

router.post("/", isAuthenticated, async (req, res) => {
    try {
        let order = new OrderPostDto(
            req.payload.id,
            req.body // 將勾選的商品的itemid跟amount用Array的形式傳過來
        );
        
        if(await Buyer.isBuyerExist(order.buyerid))
        {
            let checkResult = await Order.checkOrder(order)
            console.log(checkResult)
            if(checkResult.flag)
            {
                for(item of checkResult.order.orderDetails){
                    await ShopCart.removeItem(checkResult.order.buyerid, item.itemid)
                }
                const order = await Order.getOrder(checkResult.order._id);
                const orderReturnDto = new OrderReturnDto(order._doc)
                return res.status(201).send(orderReturnDto);
            }
            return res.status(400).json({msg: checkResult.msg});
        }

        res.status(404).send({msg: "buyer not exist"})
    } catch (e) {
        console.log(e);
        res.status(500).send("Server Error");
    }
});

router.get("/:buyerid", async (req, res) => {
    const orderGetDto = new OrderGetDto(
        req.params.buyerid, 
        req.query.skip?req.query.skip:0,
        req.query.limit?req.query.limit:10
    )
    const match = {};
    const sort = { createdAt: "desc" };

    try {
        const buyer = await Buyer.findOne({ _id: orderGetDto.buyerid });
        const totalOrders = await Order.find({buyerid: orderGetDto.buyerid}).count();
        await buyer.populate({
            path: "orders",
            match, // 匹配條件， 沒有就全部
            options: {
                limit: parseInt(orderGetDto.limit),
                skip: parseInt(orderGetDto.skip),
                sort,
            }
        });

        res.set({
            'nextSkip': parseInt(orderGetDto.skip)+parseInt(orderGetDto.limit),
            'nextLimit': orderGetDto.limit,
            'totalOrdes': totalOrders
        });

        for(order of buyer.orders)
        {
            for (orderDetail of order.orderDetails)
            {
                let itemDetail = await ProductPost.getItemById(orderDetail.itemid, '-_id name images');
                for(key of Object.keys(itemDetail._doc)){
                    orderDetail._doc[key] = itemDetail._doc[key]
                }
            }
        }

        res.send(buyer.orders);
    } catch (e) {
        console.log(e);
        res.status(500).send("Server Error");
    }
});

router.get("/boughtItems/:buyerid", async(req, res) => {
    const orderGetDto = new OrderGetDto(
        req.params.buyerid, 
        req.query.skip?req.query.skip:0,
        req.query.limit?req.query.limit:10
    )
    const sort = { createdAt: "desc" };
    try{
        //const orderDetails = await Order.getOrderDetails(orderGetDto.buyerid, orderGetDto.limit, orderGetDto.skip, sort);
        const orders = await Order.find({buyerid: orderGetDto.buyerid});
        const boughtItems = [];
        for(order of orders)
        {
            for(orderItem of order.orderDetails)
            { 
                let item = await ProductPost.getItemById(orderItem.itemid, {name: 1, price: 1, images: 1})
                //been deleted
                if(!item){
                    continue;
                }

                if(!item.images[0].startsWith('http')){
                    item.images = 'https://cf.shopee.tw/file/' + item.images[0]
                }
                item._doc.orderid = order._id;
                item._doc.amount = orderItem.amount;
                item._doc.payingStatus = order.payingStatus;
                item._doc.shippingStatus = order.orderStatus;
                item._doc.createdAt = order.createdAt.toLocaleString();
                boughtItems.push(new BoughtItem(item._doc))
            }
        }
        return res.status(200).send(boughtItems)
    }
    catch(err){
        console.log(err);
        res.status(500).send("Server Error");
    }
})

module.exports = router;