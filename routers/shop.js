const express = require("express");
const {
  isAuthenticated,
  isAuthorized,
  authorizedFor,
  accountRole,
} = require("../auth/auth.js");
const Shop = require("../models/ShopModel/shop");
const ProductPost = require("../models/ShopModel/productPost.js");
const Response = require("../models/Response");
const {
  PatchShopProfileDTO,
  PatchShopProfilePicDTO,
} = require("../DTO/PatchShopProfileDTO");
const storeShopProfilePic = require("../services/ShopProfilePicService.js");
const productPostPicPath = require("../services/ProductPostPicService.js")
const ProductPostDto = require("../DTO/ProductPostDto");
const ProductPicDto = require("../DTO/ProductPicDto");
const ModifiedProductDto = require("../DTO/ModifiedProductDto.js")
const getLabels = require("../utility/getLabelsFromShopee.js")

const router = new express.Router();

router.get(
  "/",
  isAuthenticated,
  authorizedFor(accountRole.shop),
  isAuthorized,
  (req, res) => {
    res.send("test");
  }
);

router.get("/shop/:name", isAuthenticated, async (req, res) => {
  try {
    const { name } = req.params;
    let rgx = new RegExp(name, "i");
    let shops = await Shop.find(
      {
        shopAccount: { $regex: rgx },
      },
      null,
      null
    );
    return res
      .status(200)
      .json(new Response(true, 200, "Get shops by name.", shops));
  } catch (e) {
    return res
      .status(500)
      .json(new Response(false, 500, "Internal Server Error"));
  }
});

router.get(
  "/id",
  isAuthenticated,
  authorizedFor(accountRole.shop),
  isAuthorized,
  async (req, res) => {
    try {
      const email = req.payload.email;
      const shop = await Shop.findOne({ email: email });
      return res.status(200).json(
        new Response(true, 200, "Get shop id.", {
          shopid: shop._id,
        })
      );
    } catch (err) {
      return res.status(500).json(err);
    }
  }
);

router.get("/name/:name", async (req, res) => {
  try {
    const name = req.params.name;
    const shop = await Shop.findOne({ shopAccount: name });
    if (shop) {
      return res
        .status(200)
        .json(
          new Response(true, 200, "This shop name has been taken.", {
            isTaken: true,
          })
        );
    }

    return res
      .status(200)
      .json(
        new Response(true, 200, "This shop name is available.", {
          isTaken: false,
        })
      );
  } catch (err) {
    return res.status(500).json(err);
  }
});

// Get shop Info by shopid
router.get("/:id", async (req, res) => {
  const shopid = req.params.id;
  try {
    const shop = await Shop.findOne({ _id: shopid });
    res.send(shop);
  } catch (e) {
    console.log(e);
    res.status(500).send("Server Error");
  }
});

router.patch(
  "/",
  isAuthenticated,
  authorizedFor(accountRole.shop),
  isAuthorized,
  async (req, res) => {
    try {
      var dto = new PatchShopProfileDTO(
        (id = req.body.id ?? req.payload.id),
        (name = req.body.name),
        (status = req.body.status),
        (birthday = req.body.birthday)
      );
      var shop = await Shop.findOne({ _id: dto.id });
      if (!shop) {
        return res.status(400).json(new Response(false, 400, "No shop found."));
      }

      shop.shopAccount = dto.name;
      shop.selfIntro = dto.status;
      shop.birthday = dto.birthday;
      await shop.save();

      return res
        .status(200)
        .json(
          new Response(true, 200, "Patch shop profile successfully!", shop)
        );
    } catch (e) {
      return res
        .status(500)
        .json(new Response(false, 500, "Internal Server Error"));
    }
  }
);

router.patch(
  "/profilePic",
  isAuthenticated,
  authorizedFor(accountRole.shop),
  isAuthorized,
  storeShopProfilePic.single("profilePic"),
  async (req, res) => {
    try {
      const dto = new PatchShopProfilePicDTO(
        req.shopid ?? req.payload.id,
        req.file.fieldName
      );
      var shop = await Shop.findOne({ _id: dto.shopid });
      shop.profilePic = req.fileURL;
      await shop.save();
      return res
        .status(200)
        .json(
          new Response(true, 200, "Patch shop profile pic successfully!", shop)
        );
    } catch (e) {
      return res
        .status(500)
        .json(new Response(false, 500, "Internal Server Error"));
    }
  }
);

// 獲取所有Product posts
router.get("/productPosts/:shopid", async(req, res)=>{
  try{
    var shopid = req.params.shopid;
    var posts = await ProductPost.find({shopid: shopid});
    return res.status(200).json(new Response(true, 200, "Get productposts of shop", posts));
  }catch(e){
    return res.status(500).json(new Response(false, 500, "Internal Server Error"));
  }
});

// 獲取labels
router.get("/labels/:name", async(req, res) => {
    try{
        const labels = await getLabels(req.params.name);
        res.status(200).json(labels);
    }
    catch(err){
        console.log(err)
        return res.status(500).json({ msg: "Internal Server Error" });
    }
})

// 上傳商品
router.post("/productPost", async (req, res) => {
  try {
    const body = req.body;
    const productPostDto = new ProductPostDto(
      body.shopid,
      body.shopAccount,
      body.name,
      body.content,
      body.labels,
      body.feLabels,
      body.price,
      body.stock,
      body.discount
    );

    if(!await Shop.ifShopExist(productPostDto.shopid)) {
        return res.status(404).json({msg: "shop not found"});
    }

    const productPost = new ProductPost(productPostDto)
    await productPost.save();

    return res.status(200).json(productPost);
  } catch (err) {
    console.log(err)
    return res.status(500).json({ msg: "Internal Server Error" });
  }
});

// 上傳圖片
router.patch('/productPost/pics', productPostPicPath.array("productPics", 10), async(req, res) => {
    try {
        const productFiles = new ProductPicDto(
          req.body.shopid,
          req.body.postid, // 就是/productPost回傳的_id
          req.fileURLs,
        );
  
        var post = await ProductPost.findOne({_id: productFiles.postid});
        post.images = productFiles.productPics ?? [];
        await post.save();
       
        req.fileURLs.length=0; // 清空內存
        res.status(200).json(post);
      } catch (error) {
        console.log(error);
        return res.status(500).json({ msg: "Internal Server Error" });
      }
})

// 修改商品，進入頁面先將原始資料填上，再讓使用者修改
router.patch("/productPost", async(req, res) => {
    try{
        const modifiedProduct = new ModifiedProductDto(
            req.body.postid,
            req.body.name,
            req.body.content,
            req.body.labels,
            req.body.feLabels,
            req.body.price,
            req.body.stock,
            req.body.discount
        )
        const modifiedFields = Object.assign({}, modifiedProduct);
        delete modifiedFields.postid;
        let product = await ProductPost.updateOne({_id: modifiedProduct.postid}, {...modifiedFields});
        
        const updatedProduct = await ProductPost.findOne({_id: modifiedProduct.postid}).exec();
        res.status(200).json(updatedProduct);
    }
    catch(err){
        console.log(err)
        return res.status(500).json({ msg: "Internal Server Error" });
    }
})

// 刪除貼文，先不做認證
router.delete("/productPost/:postid", async(req, res) => {
    try{
        await ProductPost.deleteOne({_id: req.params.postid});
        res.status(200).json({msg: "成功刪除貼文"})
    }
    catch(err){
        console.log(err)
        return res.status(500).json({ msg: "Internal Server Error" });
    }
})

module.exports = router;
