const express = require("express");
const { isAuthenticated, isAuthorized } = require("../auth/auth.js");
const Buyer = require("../models/BuyerModel/buyer.js");
const Order = require("../models/BuyerModel/order.js");
const ProductPost = require("../models/ShopModel/productPost.js");
const ShopCart = require("../models/BuyerModel/shopCart.js");
const ItemToCartDto = require("../DTO/ItemToCartDto.js");
const ReturnCartDto = require("../DTO/ReturnCartDto.js");
const PatchAmountReqDto = require("../DTO/PatchAmountReqDto.js");
const CartDeleteDTO = require("../DTO/CartDeleteDTO");
const router = new express.Router();

router.post("/", isAuthenticated, async (req, res) => {
  try {
    const itemToCart = new ItemToCartDto(
        req.payload.id,
        req.body.itemid,
        req.body.amount,
        req.body.cost,
        req.body.fromSharePost??null
    );
    const afterAddToCart = await ShopCart.addToCart(itemToCart)
    const cart = await ShopCart.getCarts(afterAddToCart.buyerid);
    const returnCartDto = new ReturnCartDto(cart._doc);

    return res.status(200).json(returnCartDto);

  } catch (error) {
    console.log(error);
    res.status(500).send("Server Error");
  }
});

router.get("/", isAuthenticated, async(req, res)=> {
    try{
        const cart = await ShopCart.getCarts(req.payload.id);
        const returnCartDto = new ReturnCartDto(cart._doc);
        return res.status(200).json(returnCartDto);
    }catch(error){
        console.log(error);
        res.status(500).send("Server Error");
    }
})

// 修改購物車商品數量
router.patch("/amount", isAuthenticated, async(req, res) => {
    try{
        const patchAmount = new PatchAmountReqDto(
            req.payload.id, req.body.itemid, req.body.amount
        );
        await ShopCart.modifyAmount(patchAmount);
        const cart = await ShopCart.getCarts(req.payload.id);
        const returnCartDto = new ReturnCartDto(cart._doc);
        return res.status(200).json(returnCartDto);
    }catch(error){
        console.log(error)
        res.status(500).send("Server Error");
    }
})

// 刪除item
router.delete('/:itemid', isAuthenticated, async (req, res)=>{
    try
    {  
        const cartdto = new CartDeleteDTO(
            req.payload.id,
            req.params.itemid
        );

        let cartToDelete = await ShopCart.findOne({buyerid: req.payload.id});
        cartToDelete.cartDetails = cartToDelete.cartDetails.filter(x=>x.itemid!=cartdto.itemid);
       
        await cartToDelete.save();

        const cart = await ShopCart.getCarts(req.payload.id);
        const returnCartDto = new ReturnCartDto(cart._doc);

        return res.status(200).json(returnCartDto);
    }catch(e){
        res.status(500).send("Server Error");
    }  
});

module.exports = router;
