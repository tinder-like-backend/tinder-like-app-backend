require('dotenv').config();

const express = require("express");
require("./db/tinder_app_db.js");
const productPostRouter = require("./routers/item.js");
const shopRouter = require("./routers/shop.js");
const buyerRouter = require("./routers/buyer.js");
const authRouter = require("./routers/auth.js");
const tinderRouter = require("./routers/tinder.js");
const treemapRouter = require("./routers/treemap.js");
const commentRouter = require('./routers/comment.js');
const orderRouter = require('./routers/order.js');
const shareRouter = require('./routers/share.js');
const shopCartRouter = require('./routers/shopCart.js');
const followRouter = require('./routers/follow.js')

const cors = require("cors");

const app = express();
const port = process.env.PORT || 3000;

// automatically parse incoming JSON into JS object which you can access on req.body
app.use(cors());
app.use(express.json());

//not yet use authentication to pretect route
app.use(authRouter);
app.use("/comment", commentRouter);
app.use("/shopCart", shopCartRouter);
app.use("/order", orderRouter);
app.use("/share", shareRouter);
app.use("/items", productPostRouter);
app.use("/shop", shopRouter);
app.use("/buyer", buyerRouter);
app.use("/tinder", tinderRouter)
app.use("/treemap", treemapRouter);
app.use("/follow", followRouter)


app.listen(port, () => {
  console.log("Server is up on port " + port);
});


module.exports = app;
