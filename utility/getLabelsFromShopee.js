const fetch = require('node-fetch');

const getLabels = async(productName) =>{
    const result = await fetch(`https://shopee.tw/api/v4/search/search_items?by=relevancy&keyword=${productName}&limit=60&newest=0&order=desc&page_type=search&scenario=PAGE_GLOBAL_SEARCH&version=2`);
    const body = await result.json()
    const item = await fetch(`https://shopee.tw/api/v4/item/get?itemid=${body.items[0].item_basic.itemid}&shopid=${body.items[0].item_basic.shopid}`)
    const itemJson = await item.json();
    const labels = itemJson.data.categories.map(category => {
        return {
            labelid: category.catid,
            display_name: category.display_name
        }
    });
    const fe_labels = itemJson.data.fe_categories.map(category => {
        return {
            labelid: category.catid,
            display_name: category.display_name
        }
    });
    return {
        labels: labels,
        fe_labels: fe_labels
    }
}

module.exports = getLabels;