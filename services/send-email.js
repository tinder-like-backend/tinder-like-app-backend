const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

class SendEmail
{
    constructor(to, from, subject, content, html="")
    {
        this.to = to;
        this.from = from;
        this.subject = subject;
        this.content = content
    }

    sendEmail()
    {
        let msg = {
            to: this.to, // Change to your recipient
            from: this.from, // Change to your verified sender
            subject: this.subject,
            text: this.content,
            html: this.html,
        }
          
        sgMail
        .send(msg)
        .then((response) => {
            console.log(response);
            return response;
        })
        .catch((error) => {
            console.error(error);
            return error;
        })
    }
}

module.exports=SendEmail;

