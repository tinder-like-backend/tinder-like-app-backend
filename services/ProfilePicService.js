require('dotenv').config();
const fs = require('fs-extra');
const multer = require("multer");

const picDomain = process.env.PIC_DOMAIN;

const fileFilter = require("./PicFileFilter.js");

const storage = multer.diskStorage({
  destination: function(req, file, cb){
    //cb(null, __dirname.split('services')[0]+'resources/buyer/profilePic/')
    fs.mkdirsSync(picDomain+`/buyer/${req.body.buyerid}/profile-pic`)
    const files = fs.readdirSync(picDomain+`/buyer/${req.body.buyerid}/profile-pic`)
    if(files.length > 0){
        fs.removeSync(picDomain+`/buyer/${req.body.buyerid}/profile-pic/`+files[0])
    }
    cb(null, picDomain+`/buyer/${req.body.buyerid}/profile-pic`);
  },
  filename: function(req, file, cb){
    const fileType = file.originalname.split('.')[1];
    //cb(null, file.fieldname+'_'+req.body.buyerid+'.png')
    const filename = file.fieldname+'_'+req.body.buyerid+'.'+fileType;
    req.fileURL = process.env.PIC_URL+`/buyer/${req.body.buyerid}/profile-pic/`+filename;
    cb(null, filename);
  }
})
const profilePicPath = multer({
  fileFilter: fileFilter,
  storage: storage
})


module.exports = profilePicPath