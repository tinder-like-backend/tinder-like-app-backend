require('dotenv').config();
const multer = require("multer");
const fs = require('fs-extra');

const picDomain = process.env.PIC_DOMAIN;

const fileFilter = require("./PicFileFilter.js");
let shareImgs = [];

const storage = multer.diskStorage({
  destination: function(req, file, cb){
      let dir = picDomain+`/buyer/${req.body.buyerid}/share/${req.body.postid}/`;
      fs.mkdirsSync(dir);
      cb(null, dir);
  },
  filename: function(req, file, cb){
    const filename = file.fieldname+'_'+Date.now()+'.'+file.originalname.split('.')[1];
    shareImgs.push(process.env.PIC_URL+`/buyer/${req.body.buyerid}/share/${req.body.postid}/`+filename);
    req.fileURLs = shareImgs;
    cb(null, filename);
  }
})

const sharePostPicPath = multer({
  fileFilter: fileFilter,
  storage: storage
})


module.exports = sharePostPicPath