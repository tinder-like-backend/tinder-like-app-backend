require('dotenv').config();
const multer = require("multer");
const fs = require('fs-extra');

const picDomain = process.env.PIC_DOMAIN;

const videoFilter = require("./VideoFileFilter.js");
let shareVideoes = [];

const storage = multer.diskStorage({
  destination: function(req, file, cb){
    let dir = picDomain+`/buyer/${req.body.buyerid}/share/${req.body.postid}/`;
    fs.mkdirsSync(dir)
    cb(null, dir);
  },
  filename: function(req, file, cb){
    const filename = file.fieldname+'_'+Date.now()+'.'+file.originalname.split('.')[1];
    shareVideoes.push(process.env.PIC_URL+`/buyer/${req.body.buyerid}/share/${req.body.postid}/`+filename);
    req.videoURLs = shareVideoes;
    cb(null, filename);
  }
})

const sharePostVideoPath = multer({
  fileFilter: videoFilter,
  storage: storage
})


module.exports = sharePostVideoPath