const videoFilter = function fileFilter(req, file, cb) {
    // 只接受mp4視訊格式
    if (!file.originalname.match(/\.(mp4|mov|wmv)$/)) {
      cb(new Error('Please upload an image'))
    }
    cb(null, true)
}

module.exports = videoFilter;