require('dotenv').config();
const multer = require("multer");
const fs = require('fs-extra');

const picDomain = process.env.PIC_DOMAIN;

const fileFilter = require("./PicFileFilter.js");
let productImgs = [];

const storage = multer.diskStorage({
  destination: function(req, file, cb){
      let dir = picDomain+`/shop/${req.body.shopid}/product/${req.body.postid}/`;
      fs.mkdirsSync(dir);
      cb(null, dir);
  },
  filename: function(req, file, cb){
    const filename = file.fieldname+'_'+Date.now()+'.'+file.originalname.split('.')[1];
    productImgs.push(process.env.PIC_URL+`/shop/${req.body.shopid}/product/${req.body.postid}/`+filename);
    req.fileURLs = productImgs;
    cb(null, filename);
  }
})

const productPostPicPath = multer({
  fileFilter: fileFilter,
  storage: storage
})


module.exports = productPostPicPath