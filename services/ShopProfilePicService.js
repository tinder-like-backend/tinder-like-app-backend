require('dotenv').config();
const fs = require('fs-extra');
const multer = require("multer");

const picDomain = process.env.PIC_DOMAIN;

const fileFilter = require("./PicFileFilter.js");

const storage = multer.diskStorage({
  destination: function(req, file, cb){
    //cb(null, __dirname.split('services')[0]+'resources/buyer/profilePic/')
    fs.mkdirsSync(picDomain+`/shop/${req.body.shopid}/profile-pic`)
    const files = fs.readdirSync(picDomain+`/shop/${req.body.shopid}/profile-pic`)
    if(files.length > 0){
        fs.removeSync(picDomain+`/shop/${req.body.shopid}/profile-pic/`+files[0])
    }
    cb(null, picDomain+`/shop/${req.body.shopid}/profile-pic`);
  },
  filename: function(req, file, cb){
    const fileType = file.originalname.split('.')[1];
    //cb(null, file.fieldname+'_'+req.body.buyerid+'.png')
    const filename = file.fieldname+'_'+req.body.shopid+'.'+fileType;
    req.fileURL = process.env.PIC_URL+`/shop/${req.body.shopid}/profile-pic/`+filename;
    cb(null, filename);
  }
})
const storeShopProfilePic = multer({
  fileFilter: fileFilter,
  storage: storage
})


module.exports = storeShopProfilePic