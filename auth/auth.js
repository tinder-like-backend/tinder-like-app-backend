const jwt = require("jsonwebtoken");
const config = require('../config/config.json');
const Response = require("../models/Response");

const accountRole = {
    buyer: "buyer",
    shop: "shop"
};

function isAuthenticated(req, res, next) {
    try {
        let token = req.get("authorization");
        if (!token){
            return res.status(404).json(new Response(false, 404, "Token not found."));
        }
        token = token.split(" ")[1];
        const decoded = jwt.verify(token, config.accessScret);
        
        //req.email = decoded.email;
        req.payload = decoded;

        next();
    } catch (error) {
        return res.status(401).json({ success: false, msg: error.message });
        // console.error(error);
    }
}

function verifyRefresh(email, token) {
    try {
        const decoded = jwt.verify(token, config.refreshSecret);
        return decoded.email === email;
    } catch (error) {
        // console.error(error);
        return false;
    }
}
function getRoleFromRefreshToken(token)
{
    try{
        const decoded = jwt.verify(token, config.refreshSecret);
        return decoded.role;
    }catch(error){
        return null;
    }
}   

const authorizedFor = (...roles) => {
    return (req, res, next) => {
        req.payload.acceptedRole = roles;
        next();
    };
}

function isAuthorized(req, res, next)
{
    //let route = req.originalUrl.split("/")[1]; 
    const role = req.payload.role;
    const acceptedRole = req.payload.acceptedRole;
    
    if(!acceptedRole || !acceptedRole.includes(role)){
        return res.status(403).json(new Response(false, 403, "UnAuthorized!"))
    }

    // if(route!=role){
    //     res.json(new Response(false, 403, "UnAuthorized!"))
    // }

    next();
}

module.exports = { isAuthenticated, verifyRefresh, isAuthorized, accountRole, authorizedFor, getRoleFromRefreshToken}
